package demo.ibring.de.backendstatus;

import android.content.Context;
import android.location.Geocoder;

import com.google.android.maps.GeoPoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by albert on 28/07/16.
 */
public class AddressFounder {

    public GeoPoint getLocationFromAddress(Context context, String strAddress){

        Geocoder coder = new Geocoder(context, Locale.GERMANY);
        List<android.location.Address> address = null;
        GeoPoint p1 = null;


        try {
            address = coder.getFromLocationName(strAddress,5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address==null) {
            return null;
        }
        android.location.Address location=address.get(0);
        location.getLatitude();
        location.getLongitude();

        p1 = new GeoPoint((int) (location.getLatitude() * 1E6), (int) (location.getLongitude() * 1E6));

        return p1;
    }

    public  android.location.Address location  (Context context, String strAddress){

        Geocoder coder = new Geocoder(context, Locale.GERMANY);
        List<android.location.Address> address = null;
        GeoPoint p1 = null;


        try {
            address = coder.getFromLocationName(strAddress,5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address==null) {
            return null;
        }
        android.location.Address location=address.get(0);
        location.getLatitude();
        location.getLongitude();

        return  location;
    }

    public  List<android.location.Address> locationList  (Context context, String strAddress){

        Geocoder coder = new Geocoder(context, Locale.GERMANY);
        List<android.location.Address> address = null;
        GeoPoint p1 = null;


        try {
            address = coder.getFromLocationName(strAddress,5);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address==null) {
            return null;
        }
        return  address;
    }

    public String addressShow(int i, List<android.location.Address> addressList){
        String address;
        if (addressList != null){
            address = addressList.get(i).getAddressLine(0) + " " + addressList.get(i).getAddressLine(1);
            return address;
        }else {
            return "";
        }
    }

    public List<String> addressList(Context context, String address){

        List<String> addressList = new ArrayList<String>();
        List<android.location.Address> add = locationList(context, address);
        if (add != null) {
            for (int i = 0; i < add.size(); i++) {
                addressList.add(addressShow(i, add));
                String showAdd = addressList.get(i);
            }
            return  addressList;
        }
        return null;

    }


}

