package demo.ibring.de.backendstatus.DB;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import demo.ibring.de.backendstatus.CustomersFinal;
import demo.ibring.de.backendstatus.Orders;

/**
 * Created by albert on 20/07/16.
 */
public class DBConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] classes = new Class[] {
            CustomersFinal.ItemsBean.class,
            CustomersFinal.ItemsBean.AddressesBean.class,
            CustomersFinal.ItemsBean.PhoneNumbersBean.class,
            CustomersFinal.ItemsBean.EmailAddressesBean.class,
            Orders.ItemsBean.class,
            Orders.ItemsBean.AbholadrBean.class,
            Orders.ItemsBean.AbholpartnerBean.class,
            Orders.ItemsBean.AuftraggeberBean.class,
            Orders.ItemsBean.AuftragsstatusObjBean.class,
            Orders.ItemsBean.OrderStatusLogBean.class,
            Orders.ItemsBean.ZustelladrBean.class,
            Orders.ItemsBean.ZustellpartnerBean.class};

    /**
     * To make this work in Android Studio, you may need to update your
     * Run Configuration as explained here:
     *   http://stackoverflow.com/a/17332546
     */
    public static void main(String[] args) throws IOException, SQLException {

        String currDirectory = "user.dir";

        String configPath = "/app/src/main/res/raw/ormlite_config.txt";

        /**
         * Gets the project root directory
         */
        String projectRoot = System.getProperty(currDirectory);

        /**
         * Full configuration path includes the project root path, and the location
         * of the ormlite_config.txt file appended to it
         */
        String fullConfigPath = projectRoot + configPath;

        File configFile = new File(fullConfigPath);

        /**
         * In the a scenario where we run this program serveral times, it will recreate the
         * configuration file each time with the updated configurations.
         */
        if(configFile.exists()) {
            configFile.delete();
            configFile = new File(fullConfigPath);
        }

        /**
         * writeConfigFile is a util method used to write the necessary configurations
         * to the ormlite_config.txt file.
         */
        writeConfigFile(configFile, classes);
    }
}
