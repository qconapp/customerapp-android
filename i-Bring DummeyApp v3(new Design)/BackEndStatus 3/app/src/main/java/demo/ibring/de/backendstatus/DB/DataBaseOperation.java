package demo.ibring.de.backendstatus.DB;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import demo.ibring.de.backendstatus.CustomersFinal;
import demo.ibring.de.backendstatus.DB.DatabaseHelper;
import demo.ibring.de.backendstatus.Orders;


/**
 * Created by eliebitar on 27/07/16.
 */
public class DataBaseOperation {
    String TAG = "DBop";
    DatabaseHelper databaseHelper;

    public DataBaseOperation(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    /**
     *
     * InsertOrUpdateTheTable IteamBean into the database..
     *
     * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean itemsBean) throws SQLException {

        Dao<CustomersFinal.ItemsBean, Integer> ItemsBeanDao = databaseHelper.getItemsBeanDao();



        Integer pk                     = itemsBean.getPk();
        String debitor                 = itemsBean.getDebitor();
        String klasse                  = itemsBean.getKlasse();
        String gast                    = itemsBean.getGast();
        String name1                   = itemsBean.getName1();
        String name2                   = itemsBean.getName2();
        String apartner_anrede_kennz   = itemsBean.getApartner_anrede_kennz();
        String apartner_vorname        = itemsBean.getApartner_vorname();
        String apartner_nachname       = itemsBean.getApartner_nachname();
        String geburtsdatum            = itemsBean.getGeburtsdatum();
        String zahlziel_kennz          = itemsBean.getZahlziel_kennz();
        String vertragsbeginn          = itemsBean.getVertragsbeginn();
        String ustandort_kennz         = itemsBean.getUstandort_kennz();
        String aktiv                   = itemsBean.getAktiv();

        try {
            ItemsBeanDao.createOrUpdate(new CustomersFinal.ItemsBean(
                    pk,
                    debitor,
                    klasse,
                    gast,
                    name1,
                    name2,
                    apartner_anrede_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    geburtsdatum,
                    zahlziel_kennz,
                    vertragsbeginn,
                    ustandort_kennz,
                    aktiv));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Update *****IteamBean****** into the table.......");
        }
        List<CustomersFinal.ItemsBean> list = ItemsBeanDao.queryForAll();
    }

    /*
    *
    * InsertOrUpdateTheTable customer addresses into the database
    * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean.AddressesBean addressesBean) throws SQLException {

        Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenDao = databaseHelper.getAddressesBeenDao();

        Integer ID                    = addressesBean.getID();
        Integer pk                    = addressesBean.getPk();
        Integer prio                  = addressesBean.getPrio();
        String  rechadr               = addressesBean.getRechadr();
        String  liefadr               = addressesBean.getLiefadr();
        String  zustadr               = addressesBean.getZustadr();
        String  strasse               = addressesBean.getStrasse();
        String  hausnr                = addressesBean.getHausnr();
        String  adresszusatz          = addressesBean.getAdresszusatz();
        String  plz                   = addressesBean.getPlz();
        String  ort                   = addressesBean.getOrt();
        String  land_kennz            = addressesBean.getLand_kennz();
        String  name1                 = addressesBean.getName1();
        String  name2                 = addressesBean.getName2();
        String  apartner_anrede_kennz = addressesBean.getApartner_anrede_kennz();
        String  apartner_vorname      = addressesBean.getApartner_vorname();
        String  apartner_nachname     = addressesBean.getApartner_nachname();
        double  latitude              = addressesBean.getLatitude();
        double  longitude             = addressesBean.getLongitude();


        try {
            addressesBeenDao.createOrUpdate(new CustomersFinal.ItemsBean.AddressesBean(
                    ID,
                    pk,
                    prio,
                    rechadr,
                    liefadr,
                    zustadr,
                    strasse,
                    hausnr,
                    adresszusatz,
                    plz,
                    ort,
                    land_kennz,
                    name1,
                    name2,
                    apartner_anrede_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    latitude,
                    longitude));
        }catch (Exception e){
        Log.d(TAG, "Error Inserting or Updating ******AddressesBean****** into the table.......");
        }
    }

    /*
    *
    * InsertOrUpdateTheTable Customer email into the database..
    *
    * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean.EmailAddressesBean emailAddressesBean) throws SQLException {


        Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> emailAddressesBeanIntegerDao = databaseHelper.getEmailAddressesBeanDao();


        Integer pk      = emailAddressesBean.getPk();
        String standard = emailAddressesBean.getStandard();
        String email    = emailAddressesBean.getEmail();

        try {
            emailAddressesBeanIntegerDao.createOrUpdate(new CustomersFinal.ItemsBean.EmailAddressesBean(
                    pk,
                    standard,
                    email));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******EmailAddressesBean****** into the table.......");
        }

    }

    /*
    *
    * InsertOrUpdateTheTable customer phone number into the database..
    *
    * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean.PhoneNumbersBean phoneNumbersBean) throws SQLException {

        Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> phoneNumbersBeanIntegerDao = databaseHelper.getPhoneNumbersBeanDao();



        Integer pk       = phoneNumbersBean.getPk();
        String typ       = phoneNumbersBean.getTyp();
        String standard  = phoneNumbersBean.getStandard();
        String rufnummer = phoneNumbersBean.getRufnummer();
        String bemerkung = phoneNumbersBean.getBemerkung();

        try {
            phoneNumbersBeanIntegerDao.createOrUpdate(new CustomersFinal.ItemsBean.PhoneNumbersBean(
                    pk,
                    typ,
                    standard,
                    rufnummer,
                    bemerkung));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******PhoneNumbersBean****** into the table.......");
        }

    }

    /*
    *
    * Insert OrdersIteams into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean itemsBean) throws SQLException {

        Dao<Orders.ItemsBean, Integer> mItemsBeanOrdersDao = databaseHelper.getItemsBeanOrdersDao();

        int    pk                    = itemsBean.getPk();
        int    auftragsnummer        = itemsBean.getAuftragsnummer();
        int    auftraggeber_fk       = itemsBean.getAuftraggeber_fk();
        int    abholpartner_fk       = itemsBean.getAbholpartner_fk();
        int    abholadr_fk           = itemsBean.getAbholadr_fk();
        int    zustellpartner_fk     = itemsBean.getZustellpartner_fk();
        int    zustelladr_fk         = itemsBean.getZustelladr_fk();
        String ustandort_kennz       = itemsBean.getUstandort_kennz();
        String apartner_anrede_kennz = itemsBean.getApartner_anrede_kennz();
        String apartner_vorname      = itemsBean.getApartner_vorname();
        String apartner_nachname     = itemsBean.getApartner_nachname();
        String abholen_von           = itemsBean.getAbholen_von();
        String abholen_bis           = itemsBean.getAbholen_bis();
        String abholen_wunsch        = itemsBean.getAbholen_wunsch();
        String zustellen_von         = itemsBean.getZustellen_von();
        String zustellen_bis         = itemsBean.getZustellen_bis();
        String zustellen_wunsch      = itemsBean.getZustellen_wunsch();
        String ibring_spezial_aadr   = itemsBean.getIbring_spezial_aadr();
        String ibring_spezial_zadr   = itemsBean.getIbring_spezial_zadr();
        String ibring_typ            = itemsBean.getIbring_typ();
        String sammel_hash           = itemsBean.getSammel_hash();
        String auftragsstatus        = itemsBean.getAuftragsstatus();
        String auftrag_erledigt_am   = itemsBean.getAuftrag_erledigt_am();

        try {
            mItemsBeanOrdersDao.createOrUpdate(new Orders.ItemsBean(
                    pk,
                    auftragsnummer,
                    auftraggeber_fk,
                    abholpartner_fk,
                    abholadr_fk,
                    zustellpartner_fk,
                    zustelladr_fk,
                    ustandort_kennz,
                    apartner_anrede_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    abholen_von,
                    abholen_bis,
                    abholen_wunsch,
                    zustellen_von,
                    zustellen_bis,
                    zustellen_wunsch,
                    ibring_spezial_aadr,
                    ibring_spezial_zadr,
                    ibring_typ,
                    sammel_hash,
                    auftragsstatus,
                    auftrag_erledigt_am));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******OrdersIteams****** into the table.......");
        }
    }

    /*
    *
    * Insert AuftraggeberBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.AuftraggeberBean mAuftraggeberBean) throws SQLException {

        Dao<Orders.ItemsBean.AuftraggeberBean, Integer> mAuftraggeberBeanDao = databaseHelper.getAuftraggeberBeanDao();

        Integer ID                    = mAuftraggeberBean.getID();
        int     pk                    = mAuftraggeberBean.getPk();
        String  debitor               = mAuftraggeberBean.getDebitor();
        String  klasse                = mAuftraggeberBean.getKlasse();
        String  ibring_gast           = mAuftraggeberBean.getIbring_gast();
        String  name1                 = mAuftraggeberBean.getName1();
        String  name2                 = mAuftraggeberBean.getName2();
        String  apartner_anrede_kennz = mAuftraggeberBean.getApartner_anrede_kennz();
        String  apartner_vorname      = mAuftraggeberBean.getApartner_vorname();
        String  apartner_nachname     = mAuftraggeberBean.getApartner_nachname();
        String  ustandort_kennz       = mAuftraggeberBean.getUstandort_kennz();

        try {
            mAuftraggeberBeanDao.createOrUpdate(new Orders.ItemsBean.AuftraggeberBean(
                    ID,
                    pk,
                    debitor,
                    klasse,
                    ibring_gast,
                    name1,
                    name2,
                    apartner_anrede_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    ustandort_kennz));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******AuftraggeberBean****** into the table.......");
        }
    }

    /*
    *
    * Insert AbholpartnerBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.AbholpartnerBean mAbholpartnerBean) throws SQLException {

        Dao<Orders.ItemsBean.AbholpartnerBean, Integer> mAbholpartnerBeanDao = databaseHelper.getAbholpartnerBeanDao();

        String  ustandort_kennz       = mAbholpartnerBean.getUstandort_kennz();
        String  apartner_vorname      = mAbholpartnerBean.getApartner_vorname();
        String  apartner_nachname     = mAbholpartnerBean.getApartner_nachname();
        String  apartner_anrede_kennz = mAbholpartnerBean.getApartner_anrede_kennz();
        String  name2                 = mAbholpartnerBean.getName2();
        String  name1                 = mAbholpartnerBean.getName1();
        String  ibring_gast           = mAbholpartnerBean.getIbring_gast();
        String  klasse                = mAbholpartnerBean.getKlasse();
        String  debitor               = mAbholpartnerBean.getDebitor();
        int     pk                    = mAbholpartnerBean.getPk();
        Integer ID                    = mAbholpartnerBean.getID();

        try {
            mAbholpartnerBeanDao.createOrUpdate(new Orders.ItemsBean.AbholpartnerBean(
                    ustandort_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    apartner_anrede_kennz,
                    name2,
                    name1,
                    ibring_gast,
                    klasse,
                    debitor,
                    pk,
                    ID));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******AbholpartnerBean****** into the table.......");
        }
    }

    /*
    *
    * Insert AbholadrBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.AbholadrBean mAbholadrBean) throws SQLException {

        Dao<Orders.ItemsBean.AbholadrBean, Integer> mAbholadrBeanDao = databaseHelper.getAbholadrBeanDao();

        String  apartner_nachname     = mAbholadrBean.getApartner_nachname();
        String  apartner_vorname      = mAbholadrBean.getApartner_vorname();
        String  apartner_anrede_kennz = mAbholadrBean.getApartner_anrede_kennz();
        String  apartner_name2        = mAbholadrBean.getApartner_name2();
        String  apartner_name1        = mAbholadrBean.getApartner_name1();
        String  ustandort_kennz       = mAbholadrBean.getUstandort_kennz();
        String  is_delivery_area      = mAbholadrBean.getIs_delivery_area();
        String  land_kennz            = mAbholadrBean.getLand_kennz();
        String  adresszusatz          = mAbholadrBean.getAdresszusatz();
        String  ort                   = mAbholadrBean.getOrt();
        String  plz                   = mAbholadrBean.getPlz();
        String  hausnr                = mAbholadrBean.getHausnr();
        String  strasse               = mAbholadrBean.getStrasse();
        int     pk                    = mAbholadrBean.getPk();
        Integer ID                    = mAbholadrBean.getID();

        try {
            mAbholadrBeanDao.createOrUpdate(new Orders.ItemsBean.AbholadrBean(
                    apartner_nachname,
                    apartner_vorname,
                    apartner_anrede_kennz,
                    apartner_name2,
                    apartner_name1,
                    ustandort_kennz,
                    is_delivery_area,
                    land_kennz,
                    adresszusatz,
                    ort,
                    plz,
                    hausnr,
                    strasse,
                    pk,
                    ID));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******AbholadrBean****** into the table.......");
        }
    }

    /*
    *
    * Insert ZustellpartnerBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.ZustellpartnerBean mZustellpartnerBean) throws SQLException {

        Dao<Orders.ItemsBean.ZustellpartnerBean, Integer> mZustellpartnerBeanDao = databaseHelper.getZustellpartnerBeanDao();

        String  ustandort_kennz       = mZustellpartnerBean.getUstandort_kennz();
        String  apartner_nachname     = mZustellpartnerBean.getApartner_nachname();
        String  apartner_vorname      = mZustellpartnerBean.getApartner_vorname();
        String  apartner_anrede_kennz = mZustellpartnerBean.getApartner_anrede_kennz();
        String  name2                 = mZustellpartnerBean.getName2();
        String  name1                 = mZustellpartnerBean.getName1();
        String  ibring_gast           = mZustellpartnerBean.getIbring_gast();
        String  klasse                = mZustellpartnerBean.getKlasse();
        String  debitor               = mZustellpartnerBean.getDebitor();
        int     pk                    = mZustellpartnerBean.getPk();
        Integer ID                    = mZustellpartnerBean.getID();

        try {
            mZustellpartnerBeanDao.createOrUpdate(new Orders.ItemsBean.ZustellpartnerBean(
                     ustandort_kennz,
                     apartner_nachname,
                     apartner_vorname,
                     apartner_anrede_kennz,
                     name2,
                     name1,
                     ibring_gast,
                     klasse,
                     debitor,
                     pk,
                     ID));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******ZustellpartnerBean****** into the table.......");
        }
    }

     /*
    *
    * Insert ZustelladrBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.ZustelladrBean mZustelladrBean) throws SQLException {

        Dao<Orders.ItemsBean.ZustelladrBean, Integer> mZustelladrBeanDao = databaseHelper.getZustelladrBeanDao();

        Integer ID                    = mZustelladrBean.getID();
        String  apartner_nachname     = mZustelladrBean.getApartner_nachname();
        String  apartner_vorname      = mZustelladrBean.getApartner_vorname();
        String  apartner_anrede_kennz = mZustelladrBean.getApartner_anrede_kennz();
        String  apartner_name2        = mZustelladrBean.getApartner_name2();
        String  apartner_name1        = mZustelladrBean.getApartner_name1();
        String  ustandort_kennz       = mZustelladrBean.getUstandort_kennz();
        String  is_delivery_area      = mZustelladrBean.getIs_delivery_area();
        String  land_kennz            = mZustelladrBean.getLand_kennz();
        String  adresszusatz          = mZustelladrBean.getAdresszusatz();
        String  ort                   = mZustelladrBean.getOrt();
        String  plz                   = mZustelladrBean.getPlz();
        String  hausnr                = mZustelladrBean.getHausnr();
        String  strasse               = mZustelladrBean.getStrasse();
        int     pk                    = mZustelladrBean.getPk();

        try {
            mZustelladrBeanDao.createOrUpdate(new Orders.ItemsBean.ZustelladrBean(
                    ID,
                    apartner_nachname,
                    apartner_vorname,
                    apartner_anrede_kennz,
                    apartner_name2,
                    apartner_name1,
                    ustandort_kennz,
                    is_delivery_area,
                    land_kennz,
                    adresszusatz,
                    ort,
                    plz,
                    hausnr,
                    strasse,
                    pk));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******ZustelladrBean****** into the table.......");
        }
    }

     /*
    *
    * Insert OrderItemsBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.OrderItemsBean mOrderItemsBean) throws SQLException {

        Dao<Orders.ItemsBean.OrderItemsBean, Integer> mOrderItemsBeanDao = databaseHelper.getOrderItemsBeanDao();

        String  ibring_typ        = mOrderItemsBean.getIbring_typ();
        int     gewicht_kg        = mOrderItemsBean.getGewicht_kg();
        int     hoehe_cm          = mOrderItemsBean.getHoehe_cm();
        int     breite_cm         = mOrderItemsBean.getBreite_cm();
        int     laenge_cm         = mOrderItemsBean.getLaenge_cm();
        String  label             = mOrderItemsBean.getLabel();
        String  stg_katalog_label = mOrderItemsBean.getStg_katalog_label();
        int     stg_katalog_fk    = mOrderItemsBean.getStg_katalog_fk();
        int     anzahl            = mOrderItemsBean.getAnzahl();
        int     pk                = mOrderItemsBean.getPk();
        Integer ID                = mOrderItemsBean.getAnzahl();

        try {
            mOrderItemsBeanDao.createOrUpdate(new Orders.ItemsBean.OrderItemsBean(
                    ibring_typ,
                    gewicht_kg,
                    hoehe_cm,
                    breite_cm,
                    laenge_cm,
                    label,
                    stg_katalog_label,
                    stg_katalog_fk,
                    anzahl,
                    pk,
                    ID));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******OrderItemsBean****** into the table.......");
        }
    }

     /*
    *
    * Insert AuftragsstatusObjBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.AuftragsstatusObjBean mAuftragsstatusObjBean) throws SQLException {

        Dao<Orders.ItemsBean.AuftragsstatusObjBean, Integer> mAuftragsstatusObjBeanDao = databaseHelper.getAuftragsstatusObjBeanDao();

        Integer ID                   = mAuftragsstatusObjBean.getAuftragsstatus_lfdnr();
        String  auftragsstatus       = mAuftragsstatusObjBean.getAuftragsstatus();
        int     auftragsstatus_lfdnr = mAuftragsstatusObjBean.getAuftragsstatus_lfdnr();
        String  label                = mAuftragsstatusObjBean.getLabel();
        String  bemerkung_intern     = mAuftragsstatusObjBean.getBemerkung_intern();
        String  hinweis_extern       = mAuftragsstatusObjBean.getHinweis_extern();
        String  ist_aktiv            = mAuftragsstatusObjBean.getIst_aktiv();
        String  ibring_relevant      = mAuftragsstatusObjBean.getIbring_relevant();
        String  ist_pa_aenderbar     = mAuftragsstatusObjBean.getIst_pa_aenderbar();
        String  ist_pa_stornierbar   = mAuftragsstatusObjBean.getIst_pa_stornierbar();
        String  ist_kd_aenderbar     = mAuftragsstatusObjBean.getIst_kd_aenderbar();
        String  ist_erledigt         = mAuftragsstatusObjBean.getIst_erledigt();
        String  ist_kd_stornierbar   = mAuftragsstatusObjBean.getIst_kd_stornierbar();
        String  ist_abrechenbar      = mAuftragsstatusObjBean.getIst_abrechenbar();

        try {
            mAuftragsstatusObjBeanDao.createOrUpdate(new Orders.ItemsBean.AuftragsstatusObjBean(
                    ID,
                    auftragsstatus,
                    auftragsstatus_lfdnr,
                    label,
                    bemerkung_intern,
                    hinweis_extern,
                    ist_aktiv,
                    ibring_relevant,
                    ist_pa_aenderbar,
                    ist_pa_stornierbar,
                    ist_kd_aenderbar,
                    ist_erledigt,
                    ist_kd_stornierbar,
                    ist_abrechenbar));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******AuftragsstatusObjBean****** into the table.......");
        }
    }

     /*
    *
    * Insert OrderStatusLogBean into the DATABASE..
    *
    *
    * **/

    public void InsertOrUpdateTheTable(Orders.ItemsBean.OrderStatusLogBean mOrderStatusLogBean) throws SQLException {

        Dao<Orders.ItemsBean.OrderStatusLogBean, Integer> mOrderStatusLogBeanDao = databaseHelper.getOrderStatusLogBeanDao();

        Integer ID                 = mOrderStatusLogBean.getID();
        int     pk                 = mOrderStatusLogBean.getPk();
        int     lfdnr              = mOrderStatusLogBean.getLfdnr();
        int     auftrstatus_fk     = mOrderStatusLogBean.getAuftrstatus_fk();
        String  auftragsstatus     = mOrderStatusLogBean.getAuftragsstatus();
        String  label              = mOrderStatusLogBean.getLabel();
        String  bemerkung_intern   = mOrderStatusLogBean.getBemerkung_intern();
        String  hinweis_extern     = mOrderStatusLogBean.getHinweis_extern();
        String  ist_aktiv          = mOrderStatusLogBean.getIst_aktiv();
        String  ibring_relevant    = mOrderStatusLogBean.getIbring_relevant();
        String  ist_pa_aenderbar   = mOrderStatusLogBean.getIst_pa_aenderbar();
        String  ist_pa_stornierbar = mOrderStatusLogBean.getIst_pa_stornierbar();
        String  ist_kd_aenderbar   = mOrderStatusLogBean.getIst_kd_aenderbar();
        String  ist_kd_stornierbar = mOrderStatusLogBean.getIst_kd_stornierbar();
        String  ist_erledigt       = mOrderStatusLogBean.getIst_erledigt();
        String  ist_abrechenbar    = mOrderStatusLogBean.getIst_abrechenbar();
        String  anlagedat          = mOrderStatusLogBean.getAnlagedat();

        try {
            mOrderStatusLogBeanDao.createOrUpdate(new Orders.ItemsBean.OrderStatusLogBean(
                    ID,
                    pk,
                    lfdnr,
                    auftrstatus_fk,
                    auftragsstatus,
                    label,
                    bemerkung_intern,
                    hinweis_extern,
                    ist_aktiv,
                    ibring_relevant,
                    ist_pa_aenderbar,
                    ist_pa_stornierbar,
                    ist_kd_aenderbar,
                    ist_kd_stornierbar,
                    ist_erledigt,
                    ist_abrechenbar,
                    anlagedat));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******OrderStatusLogBean****** into the table.......");
        }
    }

    /**
     *
     * Delete from table IteamBeans..
     *
     * */


    public boolean Delete(int ID, CustomersFinal.ItemsBean i) throws SQLException {
        Dao<CustomersFinal.ItemsBean, Integer> ItemsBeanDao = databaseHelper.getItemsBeanDao();
        try {
            ItemsBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG,"Error Deleting ******ItemsBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table AddressesBean..
     *
     * */

    public boolean Delete(int ID, CustomersFinal.ItemsBean.AddressesBean a) throws SQLException {
        Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenDao = databaseHelper.getAddressesBeenDao();
        try {
            addressesBeenDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG,"Error Deleting ******AddressesBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table PhoneNumbersBean..
     *
     * */

    public boolean Delete(int ID, CustomersFinal.ItemsBean.PhoneNumbersBean p) throws SQLException {
        Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> phoneNumbersBeanIntegerDao = databaseHelper.getPhoneNumbersBeanDao();
        try {
            phoneNumbersBeanIntegerDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******PhoneNumbersBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table EmailAddressesBean..
     *
     * */

    public boolean Delete(int ID, CustomersFinal.ItemsBean.EmailAddressesBean emailAddressesBean) throws SQLException {
        Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> emailAddressesBeanIntegerDao = databaseHelper.getEmailAddressesBeanDao();
        try {
            emailAddressesBeanIntegerDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******EmailAddressesBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table ItemsBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean mItemsBean) throws SQLException {
        Dao<Orders.ItemsBean, Integer> itemsBeanDao = databaseHelper.getItemsBeanOrdersDao();
        try {
            itemsBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******ItemsBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table AuftraggeberBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.AuftraggeberBean mAuftraggeberBean) throws SQLException {
        Dao<Orders.ItemsBean.AuftraggeberBean, Integer> auftraggeberBeanDao = databaseHelper.getAuftraggeberBeanDao();
        try {
            auftraggeberBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******AuftraggeberBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table AuftraggeberBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.AbholpartnerBean mAbholpartnerBean) throws SQLException {
        Dao<Orders.ItemsBean.AbholpartnerBean, Integer> abholpartnerBeanDao = databaseHelper.getAbholpartnerBeanDao();
        try {
            abholpartnerBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******AbholpartnerBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table AbholadrBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.AbholadrBean mAbholadrBean) throws SQLException {
        Dao<Orders.ItemsBean.AbholadrBean, Integer> abholadrBeanDao = databaseHelper.getAbholadrBeanDao();
        try {
            abholadrBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******AbholadrBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table ZustellpartnerBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.ZustellpartnerBean mZustellpartnerBean) throws SQLException {
        Dao<Orders.ItemsBean.ZustellpartnerBean, Integer> zustellpartnerBeanDao = databaseHelper.getZustellpartnerBeanDao();
        try {
            zustellpartnerBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******ZustellpartnerBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table ZustelladrBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.ZustelladrBean mZustelladrBean) throws SQLException {
        Dao<Orders.ItemsBean.ZustelladrBean, Integer> zustelladrBeanDao = databaseHelper.getZustelladrBeanDao();
        try {
            zustelladrBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******ZustelladrBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table AuftragsstatusObjBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.AuftragsstatusObjBean mAuftragsstatusObjBean) throws SQLException {
        Dao<Orders.ItemsBean.AuftragsstatusObjBean, Integer> auftragsstatusObjBeanDao = databaseHelper.getAuftragsstatusObjBeanDao();
        try {
            auftragsstatusObjBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******AuftragsstatusObjBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table OrderItemsBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.OrderItemsBean mOrderItemsBean) throws SQLException {
        Dao<Orders.ItemsBean.OrderItemsBean, Integer> orderItemsBeanDao = databaseHelper.getOrderItemsBeanDao();
        try {
            orderItemsBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******OrderItemsBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table OrderStatusLogBean..
     *
     * */

    public boolean Delete(int ID, Orders.ItemsBean.OrderStatusLogBean mOrderStatusLogBean) throws SQLException {
        Dao<Orders.ItemsBean.OrderStatusLogBean, Integer> OrderStatusLogBeanDao = databaseHelper.getOrderStatusLogBeanDao();
        try {
            OrderStatusLogBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******OrderStatusLogBean****** record With the ID: " + ID + " .");
            return false;
        }
    }
}
