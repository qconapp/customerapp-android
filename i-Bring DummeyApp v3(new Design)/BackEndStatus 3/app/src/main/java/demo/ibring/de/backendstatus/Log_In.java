package demo.ibring.de.backendstatus;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import demo.ibring.de.backendstatus.Address.AddressOverviewActivity;

public class Log_In extends AppCompatActivity {
    private Button register;
    private Button log_In;

    private EditText emailInput;
    private EditText passwortInput;

    private TextView forget_Info;

    private ImageView menu;

    private Validator validator;

    private String IpAddress = "";
    private String clientInfo = "";


    private Loging_In_Volley loging_in_volley;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log__in);


        validator = new Validator();
        loging_in_volley = new Loging_In_Volley();
        Gson gson = new GsonBuilder().create();

        IpAddress = validator.getIP(getApplicationContext());
        clientInfo = validator.getVersion(getApplicationContext());

        register = (Button) findViewById(R.id.to_registraion);
        log_In   = (Button) findViewById(R.id.loging_in);


        emailInput = (EditText) findViewById(R.id.email_login);
        passwortInput = (EditText) findViewById(R.id.passwort_login);


        forget_Info = (TextView) findViewById(R.id.forget_infrmation);
        menu = (ImageView) findViewById(R.id.menu_bar);

        log_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailInput.getText().toString();
                String passwort = passwortInput.getText().toString();


                // check if all field are not empty
                if (!email.isEmpty() && !passwort.isEmpty()){

                    //check if the email is valid
                    if (validator.isEmailValid(email) == true){

                        //create the JSONOBJECT
                        try {
                            JSONObject logINObject = new JSONObject("{\"email\":\""+email+"\" , \"passwort\":\""+passwort+"\"}");

                            loging_in_volley.loginPost(getApplicationContext(), logINObject, IpAddress, clientInfo, new Loging_In_Volley.logInVolleyCallback() {
                                @Override
                                public void onSuccess(String response) {
                                    try {
                                        JSONObject jResponse = new JSONObject(response);
                                        // get the status
                                        String status = jResponse.getString("status");

                                        //if status succeeded get the SessionToken And Login
                                        if (status.equalsIgnoreCase("success")){

                                            //String sessionToken = jResponse.getString("Session-Token");
                                            //validator.showErrorMessage("SessionToken is :" + sessionToken, Log_In.this);

                                            Intent intent = new Intent(Log_In.this, AddressOverviewActivity.class);
                                            startActivity(intent);

                                            //if status failed get the error
                                        }else if (status.equalsIgnoreCase("error")){
                                            validator.showErrorMessage("Wrong email or password", Log_In.this);

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                                @Override
                                public void onFail(VolleyError error) {

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //post log in request





                    }else{
                        validator.showErrorMessage("Invalid Email Address", Log_In.this);
                    }

                }else {
                    validator.showErrorMessage("Please fill all the fields", Log_In.this);
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Log_In.this, RegisterGUI.class);
                startActivity(intent);

            }
        });


        forget_Info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



    }
}
