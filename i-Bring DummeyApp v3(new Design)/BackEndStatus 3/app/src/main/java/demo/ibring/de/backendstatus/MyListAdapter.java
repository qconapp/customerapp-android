package demo.ibring.de.backendstatus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by eliebitar on 01/08/16.
 */
public class MyListAdapter extends ArrayAdapter<CustomersFinal.ItemsBean.AddressesBean> {

    LayoutInflater mInflater;
    List<CustomersFinal.ItemsBean.AddressesBean> myAddresses;
    int resource;
    Context context;

    public MyListAdapter(Context context, int resource, List<CustomersFinal.ItemsBean.AddressesBean> myAddresses) {
        super(context, resource, myAddresses);

        this.myAddresses = myAddresses;
        this.resource = resource;
        this.context = context;
        mInflater = LayoutInflater.from(this.context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View mAddress_View;
        mAddress_View = convertView;

        if (mAddress_View == null){
            mAddress_View = mInflater.inflate(R.layout.addressview, parent, false);
        }

        CustomersFinal.ItemsBean.AddressesBean currentAddress = myAddresses.get(position);

        // Fill the View.....

        TextView str = (TextView) mAddress_View.findViewById(R.id.StrInputTextView);
        str.setText(currentAddress.getStrasse());

        TextView plz = (TextView) mAddress_View.findViewById(R.id.PLZinputTextView);
        plz.setText(currentAddress.getPlz());

        TextView city = (TextView) mAddress_View.findViewById(R.id.CityInputTextView);
        city.setText(currentAddress.getOrt());


        return mAddress_View;
    }
}

