package com.example.eliebitar.i_bring_testapp;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by eliebitar on 21/07/16.
 */
@DatabaseTable(tableName = "AddressInfo_Customer")
public class AddressInfo_Customer {


        @DatabaseField(id = true)
        private Long id;

        @DatabaseField
        private String City;

        @DatabaseField

        private int PK;

        @DatabaseField
        private Date dueDate;

        public AddressInfo_Customer(int PK, String city) {
                super();
                this.PK = PK;
                City = city;
                this.dueDate = new Date(System.currentTimeMillis());
        }

        public AddressInfo_Customer(Long id) {

        }
}
