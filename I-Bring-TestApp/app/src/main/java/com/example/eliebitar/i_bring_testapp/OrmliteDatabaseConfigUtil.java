package com.example.eliebitar.i_bring_testapp;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by eliebitar on 21/07/16.
 */
public class OrmliteDatabaseConfigUtil  extends OrmLiteConfigUtil {


    private static final Class<?>[] classes = new Class[] {AddressInfo_Customer.class};


    public static void main(String[] args) throws java.sql.SQLException, IOException {

        String currDirectory = "user.dir";

        String configPath = "/app/src/main/res/raw/ormlite_config.txt";

        String projectRoot = System.getProperty(currDirectory);

        String fullConfigPath = projectRoot + configPath;

        File configFile = new File(fullConfigPath);

        if(configFile.exists()) {
            configFile.delete();
            configFile = new File(fullConfigPath);
        }

        writeConfigFile(configFile, classes);
    }
}