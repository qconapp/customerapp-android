
package demo.ibring.de.ibringdummyappv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CustomerItems {

    @SerializedName("items")
    @Expose
    private List<Customer> customers = new ArrayList<Customer>();

    /**
     * 
     * @return
     *     The items
     */
    public List<Customer> getCustomers() {
        return customers;
    }

    /**
     * 
     * @param items
     *     The items
     */
    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

}
