package demo.ibring.de.ibringdummyappv2;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ScrollingView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.maps.GeoPoint;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RegisterGUI extends AppCompatActivity{

    private RadioGroup gender_group;
    private RadioGroup klasse_group;
    private RadioButton privat;
    private RadioButton firma;
    private RadioButton herr;
    private RadioButton frau;
    private CheckBox stimmung;


    private EditText inputVorname;
    private EditText inputNachname;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText repeatePassword;
    private EditText inputPhone;
    private EditText inputDate;
    private EditText inputFirmaName;
    private EditText inputAbteilung;
    private EditText inputSteuerId;
    private EditText inputAdditionalAddress;
    private EditText inputStreet;
    private EditText inputStreetNr;
    private EditText inputPLZ;
    private EditText inputCity;
    private EditText inputLand;

    private TextView firmaText;
    private TextView steuerText;
    private TextView abteilungText;

    private Button to_Partner;
    private Button register;
    private ImageView toLogIn;

    private String gender = "";
    private String landInput = "DE";

    private Validator validator;
    private AddressFounder addressFounder;
    private RegisterGSON registerJSON ;
    private ErrorHandler errorHandler;


    ScrollView scrollView;

    Drawable checkIcon;
    Drawable exIcon;



    Calendar myCalendar = Calendar.getInstance();

    RegisterVolley registerVolley;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        validator = new Validator();
        addressFounder = new AddressFounder();
        errorHandler = new ErrorHandler();
        final Gson gson = new GsonBuilder().create();

        checkIcon = getResources().getDrawable(R.drawable.check_ibring);
        exIcon = getResources().getDrawable(R.drawable.exibring);

        checkIcon.setBounds(0,0,checkIcon.getIntrinsicWidth(), checkIcon.getIntrinsicHeight());
        exIcon.setBounds(0,0,exIcon.getIntrinsicWidth(), exIcon.getIntrinsicHeight());










        gender_group = (RadioGroup) findViewById(R.id.radio_group);
        klasse_group = (RadioGroup) findViewById(R.id.radio_group_klass);

        herr = (RadioButton) findViewById(R.id.herr);
        frau  = (RadioButton) findViewById(R.id.frau);
        privat = (RadioButton) findViewById(R.id.radio_privat);
        firma = (RadioButton) findViewById(R.id.radio_geschaftlich);
        stimmung = (CheckBox) findViewById(R.id.kunder_terms);

        inputVorname = (EditText) findViewById(R.id.kunde_vorname);
        validator.focusLisnet(inputVorname,"","Please enter a First Name", exIcon, checkIcon);
        inputNachname = (EditText) findViewById(R.id.kunde_nachname);
        validator.focusLisnet(inputNachname,"","Please enter a Last Name", exIcon, checkIcon);
        inputEmail = (EditText) findViewById(R.id.kunde_email);
        validator.emailfocusLisnet(inputEmail,"", "Please enter an Email Address ",exIcon,checkIcon);
        inputPassword = (EditText) findViewById(R.id.kunde_password);
        validator.passwordfocusLisnet(inputPassword,"","Please enter a password that contains at least 8 characters and big and small letters", exIcon, checkIcon);
        repeatePassword = (EditText) findViewById(R.id.kunde_password_repeat);
        validator.passwordRepeatfocusLisnet(inputPassword,repeatePassword,"","Please repeate your paessword",exIcon,checkIcon);
        inputPhone = (EditText) findViewById(R.id.kunde_phone);
        validator.focusLisnet(inputPhone,"","Please enter phone number", exIcon, checkIcon);
        inputDate = (EditText) findViewById(R.id.kunde_gebustdatum);
        final DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                updateLabel();
            }
        };
        //validator.datefocusLisnet(inputDate,"","Please enter your birth Date in the flfflowing format TT.MM.YYYY", exIcon,checkIcon);
        inputDate.setInputType(InputType.TYPE_NULL);
        inputDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(RegisterGUI.this, datePicker, myCalendar.get(Calendar.DAY_OF_MONTH), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.YEAR)).show();
                }

            }
        });
        inputFirmaName = (EditText) findViewById(R.id.kunder_firmaname);
        validator.focusLisnet(inputFirmaName,"","Please enter a Firma name", exIcon, checkIcon);
        inputAbteilung = (EditText) findViewById(R.id.kunde_abteilung);
        validator.focusLisnet(inputAbteilung,"","Please enter your Abteilung", exIcon, checkIcon);
        inputSteuerId = (EditText) findViewById(R.id.kunde_steuer);
        validator.focusLisnet(inputSteuerId,"","Please enter your SteuerId", exIcon, checkIcon);
        inputAdditionalAddress = (EditText) findViewById(R.id.kunder_Addtional_address);
        validator.focusLisnet(inputAdditionalAddress,"","Please enter addtional address details ", exIcon, checkIcon);
        inputStreet = (EditText) findViewById(R.id.kunde_strasse);
        validator.focusLisnet(inputStreet,"","Please enter your street", exIcon, checkIcon);
        inputStreetNr = (EditText) findViewById(R.id.kunde_strassenummer);
        validator.focusLisnet(inputStreetNr,"","Please enter your street number", exIcon, checkIcon);
        inputPLZ = (EditText) findViewById(R.id.kunde_plz);
        validator.focusLisnet(inputPLZ,"","Please enter your PLZ", exIcon, checkIcon);
        inputCity = (EditText) findViewById(R.id.kunde_ort);
        validator.focusLisnet(inputCity,"","Please enter your City", exIcon, checkIcon);
        inputLand = (EditText) findViewById(R.id.kunde_land);
        validator.focusLisnet(inputLand,"","Please enter your Land", exIcon, checkIcon);

        scrollView = (ScrollView) findViewById(R.id.scrollView);



        to_Partner = (Button) findViewById(R.id.to_partner);
        register = (Button) findViewById(R.id.kunder_registering);
        toLogIn = (ImageView) findViewById(R.id.back_to_login);

        steuerText = (TextView) findViewById(R.id.text_steuer);
        abteilungText = (TextView)findViewById(R.id.text_abteilung);
        firmaText = (TextView)findViewById(R.id.text_firma);



        inputDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                new DatePickerDialog(RegisterGUI.this, datePicker, myCalendar.get(Calendar.DAY_OF_MONTH), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.YEAR)).show();
            }
        });


        klasse_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (privat.isChecked()){
                    inputAbteilung.setVisibility(View.GONE);
                    inputSteuerId.setVisibility(View.GONE);
                    inputFirmaName.setVisibility(View.GONE);

                    steuerText.setVisibility(View.GONE);
                    abteilungText.setVisibility(View.GONE);
                    firmaText.setVisibility(View.GONE);


                }
                if (firma.isChecked()){
                    inputAbteilung.setVisibility(View.VISIBLE);
                    inputSteuerId.setVisibility(View.VISIBLE);
                    inputFirmaName.setVisibility(View.VISIBLE);

                    steuerText.setVisibility(View.VISIBLE);
                    abteilungText.setVisibility(View.VISIBLE);
                    firmaText.setVisibility(View.VISIBLE);

                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (herr.isChecked()){
                    gender = "HR";
                }else{
                    gender = "FR";
                }

                String vorname = inputVorname.getText().toString();
                String nachname = inputNachname.getText().toString();
                final String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();
                String password_repeat = repeatePassword.getText().toString();
                String phone = inputPhone.getText().toString();
                String birthDate = inputDate.getText().toString();
                String additionalAddress = inputAdditionalAddress.getText().toString();
                String street = inputStreet.getText().toString();
                String streetNumber = inputStreetNr.getText().toString();
                String plz = inputPLZ.getText().toString();
                String city = inputCity.getText().toString();
                String land = inputLand.getText().toString();
                String steuer_id = inputSteuerId.getText().toString();
                String abteilung = inputAbteilung.getText().toString();
                String fimaname = inputFirmaName.getText().toString();
                EditText[] privateEditText  = {inputVorname,inputNachname,inputDate,inputStreet,inputStreetNr,inputAdditionalAddress,inputPLZ,inputCity, inputLand, inputEmail,inputPassword,repeatePassword,inputPhone };
                EditText[] firmaEditText =  {inputVorname,inputNachname,inputDate,inputStreet,inputStreetNr,inputAdditionalAddress,inputPLZ,inputCity, inputLand, inputEmail,inputPassword,repeatePassword,inputPhone,inputFirmaName,inputAbteilung, inputSteuerId};
                //boolean notEmbty = validator.validateEditText(privateEditText);


                if (validator.isNetworkAvailable(getApplicationContext()) == true){

                    if (herr.isChecked() || frau.isChecked()){
                        if (privat.isChecked() || firma.isChecked()){
                            if (privat.isChecked()){
                                if (validator.validateEditText(privateEditText)== true){
                                    if (validator.isEmailValid(email)){
                                        if (validator.isValidPassword(password)){
                                            if (password.equals(password_repeat)){
                                                if (validator.isValidDate(birthDate)){
                                                    if (stimmung.isChecked()) {
                                                        registerVolley = new RegisterVolley();
                                                        android.location.Address geoPoint = addressFounder.location(getApplicationContext(), street + " " + streetNumber + " " + plz + " " + city);
                                                        double latitude = geoPoint.getLatitude();
                                                        double longitude = geoPoint.getLongitude();
                                                        registerJSON = new RegisterGSON();
                                                        registerJSON.setKlasse("P");
                                                        registerJSON.setName1(vorname + " " + nachname);
                                                        registerJSON.setName2("");
                                                        registerJSON.setApartnerAnredeKennz(gender);
                                                        registerJSON.setApartnerVorname(vorname);
                                                        registerJSON.setApartnerNachname(nachname);
                                                        registerJSON.setGeburtsdatum(birthDate);
                                                        registerJSON.setStrasse(street);
                                                        registerJSON.setHausnr(streetNumber);
                                                        registerJSON.setAdresszusatz(additionalAddress);
                                                        registerJSON.setPlz(plz);
                                                        registerJSON.setOrt(city);
                                                        registerJSON.setLandKennz(landInput);
                                                        registerJSON.setRufnummer(phone);
                                                        registerJSON.setEmail(email);
                                                        registerJSON.setPasswort(password);
                                                        registerJSON.setLatitude(latitude);
                                                        registerJSON.setLongitude(longitude);
                                                        String fullAddress =street + " " + streetNumber + " " + plz + " " + city;


                                                        final List<String> addressList = addressFounder.addressList(getApplicationContext(), fullAddress);
                                                        if (addressList != null){
                                                            final CharSequence address[] = addressList.toArray(new CharSequence[addressList.size()]);
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterGUI.this);
                                                            builder.setView(R.layout.alert_dialog);
                                                            builder.setTitle("   Did you mean");
                                                            builder.setItems(address, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    String newAddress = addressList.get(which);
                                                                    android.location.Address geoPoint = addressFounder.location(getApplicationContext(),newAddress);

                                                                    registerJSON.setStrasse(geoPoint.getThoroughfare());
                                                                    registerJSON.setHausnr(geoPoint.getSubThoroughfare());
                                                                    registerJSON.setPlz(geoPoint.getPostalCode());
                                                                    registerJSON.setOrt(geoPoint.getLocality());
                                                                    registerJSON.setLatitude((double) geoPoint.getLatitude());
                                                                    registerJSON.setLongitude((double) geoPoint.getLongitude());

                                                                    String registerOut = gson.toJson(registerJSON, RegisterGSON.class);
                                                                    try {
                                                                        JSONObject registerJson = new JSONObject(registerOut);
                                                                        registerVolley.registerUser(getApplicationContext(), registerJson, new RegisterVolley.registerVolleyCallback() {
                                                                            @Override
                                                                            public void onSuccess(String response) {
                                                                                try {
                                                                                    JSONObject responseJSON = new JSONObject(response);
                                                                                    String status = responseJSON.getString("status");
                                                                                    if (status.equalsIgnoreCase("success")) {
                                                                                        validator.showErrorMessage("SUCCESS", RegisterGUI.this);
                                                                                    } else if (status.equalsIgnoreCase("error")) {
                                                                                        String errorMSG = responseJSON.getString("error_message");
                                                                                        List<String> error_msgs = errorHandler.errorList(responseJSON);
                                                                                        if (errorHandler.emailExist(error_msgs) == true){
                                                                                            validator.showErrorMessage("E-Mail Addrerss already Exists", getApplicationContext());
                                                                                        }else {
                                                                                            validator.showErrorMessage(errorMSG, RegisterGUI.this);
                                                                                        }
                                                                                    }
                                                                                } catch (JSONException e) {
                                                                                    e.printStackTrace();
                                                                                }

                                                                            }

                                                                            @Override
                                                                            public void onFail(VolleyError error) {

                                                                            }
                                                                        });
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            });
                                                            builder.show();
                                                        }else {
                                                            String registerOut = gson.toJson(registerJSON, RegisterGSON.class);
                                                            try {
                                                                JSONObject registerJson = new JSONObject(registerOut);
                                                                registerVolley.registerUser(getApplicationContext(), registerJson, new RegisterVolley.registerVolleyCallback() {
                                                                    @Override
                                                                    public void onSuccess(String response) {
                                                                        try {
                                                                            JSONObject responseJSON = new JSONObject(response);
                                                                            String status = responseJSON.getString("status");
                                                                            if (status.equalsIgnoreCase("success")) {
                                                                                validator.showErrorMessage("SUCCESS", RegisterGUI.this);
                                                                            } else if (status.equalsIgnoreCase("error")) {
                                                                                String errorMSG = responseJSON.getString("error_message");
                                                                                List<String> error_msgs = errorHandler.errorList(responseJSON);
                                                                                if (errorHandler.emailExist(error_msgs) == true) {
                                                                                    validator.showErrorMessage("E-Mail Addrerss already Exists", getApplicationContext());
                                                                                } else {
                                                                                    validator.showErrorMessage(errorMSG, RegisterGUI.this);
                                                                                }
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                        }

                                                                    }

                                                                    @Override
                                                                    public void onFail(VolleyError error) {

                                                                    }
                                                                });
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }else{
                                                        validator.showErrorMessage("Please Accept terms and conditions", RegisterGUI.this);
                                                    }

                                                }else {

                                                    inputDate.setError("Invalid Date or Date format. Date format must be TT.MM.YYYY", exIcon);
                                                    inputDate.setFocusable(true);
                                                }
                                            }else{

                                                repeatePassword.setError("Password does not match", exIcon);
                                                repeatePassword.setFocusable(true);

                                            }
                                        }else{

                                            inputPassword.setError("Invalid password. password must be at least 8 characters and contain big and small letters ", exIcon);
                                            inputPassword.setFocusable(true);
                                        }
                                    }else{

                                        inputEmail.setError("Invalid Email Address", exIcon);
                                        inputEmail.setFocusable(true);
                                    }

                                }else{

                                    //inputVorname.setError("Please Fill name");
                                    //inputVorname.setFocusable(true);
                                    //
                                    validator.showErrorMessage("please fill all the fields", RegisterGUI.this);

                                }

                            }else if (firma.isChecked()){
                                if (validator.validateEditText(firmaEditText) == true ){
                                    if (validator.isEmailValid(email)){
                                        if (validator.isValidPassword(password)){
                                            if (password.equals(password_repeat)){
                                                if (validator.isValidDate(birthDate)){
                                                    if (stimmung.isChecked()) {
                                                        registerVolley = new RegisterVolley();
                                                        android.location.Address geoPoint = addressFounder.location(getApplicationContext(), street + " " + streetNumber + " , " + plz + " , " + city);
                                                        double latitude = geoPoint.getLatitude();
                                                        double longitude = geoPoint.getLongitude();
                                                        registerJSON = new RegisterGSON();
                                                        registerJSON.setKlasse("G");
                                                        registerJSON.setName1(fimaname);
                                                        registerJSON.setName2(abteilung);
                                                        registerJSON.setApartnerAnredeKennz(gender);
                                                        registerJSON.setApartnerVorname(vorname);
                                                        registerJSON.setApartnerNachname(nachname);
                                                        registerJSON.setGeburtsdatum(birthDate);
                                                        registerJSON.setStrasse(street);
                                                        registerJSON.setHausnr(streetNumber);
                                                        registerJSON.setAdresszusatz(additionalAddress);
                                                        registerJSON.setPlz(plz);
                                                        registerJSON.setOrt(city);
                                                        registerJSON.setLandKennz(landInput);
                                                        registerJSON.setRufnummer(phone);
                                                        registerJSON.setEmail(email);
                                                        registerJSON.setPasswort(password);
                                                        registerJSON.setLatitude(latitude);
                                                        registerJSON.setLongitude(longitude);
                                                        registerJSON.setUstid(steuer_id);

                                                        String fullAddress =street + " " + streetNumber + " " + plz + " " + city;


                                                        final List<String> addressList = addressFounder.addressList(getApplicationContext(), fullAddress);
                                                        if (addressList != null){
                                                            final CharSequence address[] = addressList.toArray(new CharSequence[addressList.size()]);
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterGUI.this);
                                                            builder.setView(R.layout.alert_dialog);
                                                            builder.setTitle("   Did you mean");
                                                            builder.setItems(address, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    String newAddress = addressList.get(which);
                                                                    android.location.Address geoPoint = addressFounder.location(getApplicationContext(),newAddress);

                                                                    registerJSON.setStrasse(geoPoint.getThoroughfare());
                                                                    registerJSON.setHausnr(geoPoint.getSubThoroughfare());
                                                                    registerJSON.setPlz(geoPoint.getPostalCode());
                                                                    registerJSON.setOrt(geoPoint.getLocality());
                                                                    registerJSON.setLatitude((double) geoPoint.getLatitude());
                                                                    registerJSON.setLongitude((double) geoPoint.getLongitude());

                                                                    String registerOut = gson.toJson(registerJSON, RegisterGSON.class);
                                                                    try {
                                                                        JSONObject registerJson = new JSONObject(registerOut);
                                                                        registerVolley.registerUser(getApplicationContext(), registerJson, new RegisterVolley.registerVolleyCallback() {
                                                                            @Override
                                                                            public void onSuccess(String response) {
                                                                                try {
                                                                                    JSONObject responseJSON = new JSONObject(response);
                                                                                    String status = responseJSON.getString("status");
                                                                                    if (status.equalsIgnoreCase("success")) {
                                                                                        validator.showErrorMessage("SUCCESS", RegisterGUI.this);
                                                                                    } else if (status.equalsIgnoreCase("error")) {
                                                                                        String errorMSG = responseJSON.getString("error_message");
                                                                                        List<String> error_msgs = errorHandler.errorList(responseJSON);
                                                                                        if (errorHandler.emailExist(error_msgs) == true){
                                                                                            validator.showErrorMessage("E-Mail Addrerss already Exists", getApplicationContext());
                                                                                        }else {
                                                                                            validator.showErrorMessage(errorMSG, RegisterGUI.this);
                                                                                        }
                                                                                    }
                                                                                } catch (JSONException e) {
                                                                                    e.printStackTrace();
                                                                                }

                                                                            }

                                                                            @Override
                                                                            public void onFail(VolleyError error) {

                                                                            }
                                                                        });
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            });
                                                            builder.show();
                                                        }

                                                        String registerOut = gson.toJson(registerJSON, RegisterGSON.class);
                                                        try {
                                                            JSONObject registerJson = new JSONObject(registerOut);
                                                            registerVolley.registerUser(getApplicationContext(), registerJson, new RegisterVolley.registerVolleyCallback() {
                                                                @Override
                                                                public void onSuccess(String response) {
                                                                    try {
                                                                        JSONObject responseJSON = new JSONObject(response);
                                                                        String status = responseJSON.getString("status");
                                                                        if (status.equalsIgnoreCase("success")) {
                                                                            validator.showErrorMessage("SUCCESS", RegisterGUI.this);
                                                                        } else if (status.equalsIgnoreCase("error")) {
                                                                            String errorMSG = responseJSON.getString("error_message");
                                                                            List<String> error_msgs = errorHandler.errorList(responseJSON);
                                                                            if (errorHandler.emailExist(error_msgs) == true){
                                                                                validator.showErrorMessage("E-Mail Addrerss already Exists", getApplicationContext());
                                                                            }else {
                                                                                validator.showErrorMessage(errorMSG, RegisterGUI.this);
                                                                            }
                                                                        }
                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }

                                                                }

                                                                @Override
                                                                public void onFail(VolleyError error) {

                                                                }
                                                            });
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }else {
                                                        validator.showErrorMessage("Please Accept terms and conditions", RegisterGUI.this);

                                                    }
                                                }else {

                                                    inputDate.setError("Invalid Date or Date format. Date format must be TT.MM.YYYY", exIcon);
                                                    inputDate.setFocusable(true);
                                                }
                                            }else{

                                                repeatePassword.setError("Password does not match");
                                                repeatePassword.setFocusable(true);

                                            }
                                        }else{

                                            inputPassword.setError("Invalid password. password must be at least 8 characters and contain big and small letters ", exIcon);
                                            inputPassword.setFocusable(true);
                                        }
                                    }else{

                                        inputEmail.setError("Invalid Email Address", exIcon);
                                        inputEmail.setFocusable(true);

                                        //validator.showErrorMessage("INVALID email", RegisterGUI.this);
                                    }

                                }else{
                                    validator.showErrorMessage("please fill all the fields", RegisterGUI.this);
                                }

                            }
                        }else{
                            validator.showErrorMessage("please choose a klass", RegisterGUI.this);
                        }

                    }else{
                        validator.showErrorMessage("please choose a gender", RegisterGUI.this);
                    }

                }


            }
        });

        toLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterGUI.this, Log_In.class);
                startActivity(intent);
            }
        });




    }





    private void updateLabel() {

        String myFormat = "dd.MM.yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMANY);

        inputDate.setText(sdf.format(myCalendar.getTime()));
        inputStreet.requestFocus();
       // int test = inputDate.getNextFocusDownId();
       // findViewById(inputDate.getNextFocusDownId()).requestFocus();
    }







}
