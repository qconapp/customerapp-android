
package demo.ibring.de.ibringdummyappv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneNumber {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("typ")
    @Expose
    private String typ;
    @SerializedName("standard")
    @Expose
    private String standard;
    @SerializedName("rufnummer")
    @Expose
    private String rufnummer;
    @SerializedName("bemerkung")
    @Expose
    private String bemerkung;

    /**
     * 
     * @return
     *     The pk
     */
    public Integer getPk() {
        return pk;
    }

    /**
     * 
     * @param pk
     *     The pk
     */
    public void setPk(Integer pk) {
        this.pk = pk;
    }

    /**
     * 
     * @return
     *     The typ
     */
    public String getTyp() {
        return typ;
    }

    /**
     * 
     * @param typ
     *     The typ
     */
    public void setTyp(String typ) {
        this.typ = typ;
    }

    /**
     * 
     * @return
     *     The standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * 
     * @param standard
     *     The standard
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * 
     * @return
     *     The rufnummer
     */
    public String getRufnummer() {
        return rufnummer;
    }

    /**
     * 
     * @param rufnummer
     *     The rufnummer
     */
    public void setRufnummer(String rufnummer) {
        this.rufnummer = rufnummer;
    }

    /**
     * 
     * @return
     *     The bemerkung
     */
    public String getBemerkung() {
        return bemerkung;
    }

    /**
     * 
     * @param bemerkung
     *     The bemerkung
     */
    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

}
