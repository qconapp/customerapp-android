
package demo.ibring.de.ibringdummyappv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("debitor")
    @Expose
    private String debitor;
    @SerializedName("klasse")
    @Expose
    private String klasse;
    @SerializedName("gast")
    @Expose
    private String gast;
    @SerializedName("name1")
    @Expose
    private String name1;
    @SerializedName("name2")
    @Expose
    private String name2;
    @SerializedName("apartner_anrede_kennz")
    @Expose
    private String apartnerAnredeKennz;
    @SerializedName("apartner_vorname")
    @Expose
    private String apartnerVorname;
    @SerializedName("apartner_nachname")
    @Expose
    private String apartnerNachname;
    @SerializedName("geburtsdatum")
    @Expose
    private String geburtsdatum;
    @SerializedName("zahlziel_kennz")
    @Expose
    private String zahlzielKennz;
    @SerializedName("vertragsbeginn")
    @Expose
    private String vertragsbeginn;
    @SerializedName("ustandort_kennz")
    @Expose
    private String ustandortKennz;
    @SerializedName("related_customer")
    @Expose
    private Object relatedCustomer;
    @SerializedName("related_customer_kennz")
    @Expose
    private String relatedCustomerKennz;
    @SerializedName("aktiv")
    @Expose
    private String aktiv;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = new ArrayList<Address>();
    @SerializedName("email_addresses")
    @Expose
    private List<EmailAddress> emailAddresses = new ArrayList<EmailAddress>();
    @SerializedName("phone_numbers")
    @Expose
    private List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
    @SerializedName("open_hours")
    @Expose
    private List<Object> openHours = new ArrayList<Object>();
    @SerializedName("description")
    @Expose
    private List<Object> description = new ArrayList<Object>();

    /**
     * 
     * @return
     *     The pk
     */
    public Integer getPk() {
        return pk;
    }

    /**
     * 
     * @param pk
     *     The pk
     */
    public void setPk(Integer pk) {
        this.pk = pk;
    }

    /**
     * 
     * @return
     *     The debitor
     */
    public String getDebitor() {
        return debitor;
    }

    /**
     * 
     * @param debitor
     *     The debitor
     */
    public void setDebitor(String debitor) {
        this.debitor = debitor;
    }

    /**
     * 
     * @return
     *     The klasse
     */
    public String getKlasse() {
        return klasse;
    }

    /**
     * 
     * @param klasse
     *     The klasse
     */
    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }

    /**
     * 
     * @return
     *     The gast
     */
    public String getGast() {
        return gast;
    }

    /**
     * 
     * @param gast
     *     The gast
     */
    public void setGast(String gast) {
        this.gast = gast;
    }

    /**
     * 
     * @return
     *     The name1
     */
    public String getName1() {
        return name1;
    }

    /**
     * 
     * @param name1
     *     The name1
     */
    public void setName1(String name1) {
        this.name1 = name1;
    }

    /**
     * 
     * @return
     *     The name2
     */
    public String getName2() {
        return name2;
    }

    /**
     * 
     * @param name2
     *     The name2
     */
    public void setName2(String name2) {
        this.name2 = name2;
    }

    /**
     * 
     * @return
     *     The apartnerAnredeKennz
     */
    public String getApartnerAnredeKennz() {
        return apartnerAnredeKennz;
    }

    /**
     * 
     * @param apartnerAnredeKennz
     *     The apartner_anrede_kennz
     */
    public void setApartnerAnredeKennz(String apartnerAnredeKennz) {
        this.apartnerAnredeKennz = apartnerAnredeKennz;
    }

    /**
     * 
     * @return
     *     The apartnerVorname
     */
    public String getApartnerVorname() {
        return apartnerVorname;
    }

    /**
     * 
     * @param apartnerVorname
     *     The apartner_vorname
     */
    public void setApartnerVorname(String apartnerVorname) {
        this.apartnerVorname = apartnerVorname;
    }

    /**
     * 
     * @return
     *     The apartnerNachname
     */
    public String getApartnerNachname() {
        return apartnerNachname;
    }

    /**
     * 
     * @param apartnerNachname
     *     The apartner_nachname
     */
    public void setApartnerNachname(String apartnerNachname) {
        this.apartnerNachname = apartnerNachname;
    }

    /**
     * 
     * @return
     *     The geburtsdatum
     */
    public String getGeburtsdatum() {
        return geburtsdatum;
    }

    /**
     * 
     * @param geburtsdatum
     *     The geburtsdatum
     */
    public void setGeburtsdatum(String geburtsdatum) {
        this.geburtsdatum = geburtsdatum;
    }

    /**
     * 
     * @return
     *     The zahlzielKennz
     */
    public String getZahlzielKennz() {
        return zahlzielKennz;
    }

    /**
     * 
     * @param zahlzielKennz
     *     The zahlziel_kennz
     */
    public void setZahlzielKennz(String zahlzielKennz) {
        this.zahlzielKennz = zahlzielKennz;
    }

    /**
     * 
     * @return
     *     The vertragsbeginn
     */
    public String getVertragsbeginn() {
        return vertragsbeginn;
    }

    /**
     * 
     * @param vertragsbeginn
     *     The vertragsbeginn
     */
    public void setVertragsbeginn(String vertragsbeginn) {
        this.vertragsbeginn = vertragsbeginn;
    }

    /**
     * 
     * @return
     *     The ustandortKennz
     */
    public String getUstandortKennz() {
        return ustandortKennz;
    }

    /**
     * 
     * @param ustandortKennz
     *     The ustandort_kennz
     */
    public void setUstandortKennz(String ustandortKennz) {
        this.ustandortKennz = ustandortKennz;
    }

    /**
     * 
     * @return
     *     The relatedCustomer
     */
    public Object getRelatedCustomer() {
        return relatedCustomer;
    }

    /**
     * 
     * @param relatedCustomer
     *     The related_customer
     */
    public void setRelatedCustomer(Object relatedCustomer) {
        this.relatedCustomer = relatedCustomer;
    }

    /**
     * 
     * @return
     *     The relatedCustomerKennz
     */
    public String getRelatedCustomerKennz() {
        return relatedCustomerKennz;
    }

    /**
     * 
     * @param relatedCustomerKennz
     *     The related_customer_kennz
     */
    public void setRelatedCustomerKennz(String relatedCustomerKennz) {
        this.relatedCustomerKennz = relatedCustomerKennz;
    }

    /**
     * 
     * @return
     *     The aktiv
     */
    public String getAktiv() {
        return aktiv;
    }

    /**
     * 
     * @param aktiv
     *     The aktiv
     */
    public void setAktiv(String aktiv) {
        this.aktiv = aktiv;
    }

    /**
     * 
     * @return
     *     The addresses
     */
    public List<Address> getAddresses() {
        return addresses;
    }

    /**
     * 
     * @param addresses
     *     The addresses
     */
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     * 
     * @return
     *     The emailAddresses
     */
    public List<EmailAddress> getEmailAddresses() {
        return emailAddresses;
    }

    /**
     * 
     * @param emailAddresses
     *     The email_addresses
     */
    public void setEmailAddresses(List<EmailAddress> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    /**
     * 
     * @return
     *     The phoneNumbers
     */
    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    /**
     * 
     * @param phoneNumbers
     *     The phone_numbers
     */
    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    /**
     * 
     * @return
     *     The openHours
     */
    public List<Object> getOpenHours() {
        return openHours;
    }

    /**
     * 
     * @param openHours
     *     The open_hours
     */
    public void setOpenHours(List<Object> openHours) {
        this.openHours = openHours;
    }

    /**
     * 
     * @return
     *     The description
     */
    public List<Object> getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(List<Object> description) {
        this.description = description;
    }

}
