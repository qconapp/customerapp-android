
package demo.ibring.de.ibringdummyappv2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {
    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("prio")
    @Expose
    private Integer prio;
    @SerializedName("rechadr")
    @Expose
    private String rechadr;
    @SerializedName("liefadr")
    @Expose
    private String liefadr;
    @SerializedName("zustadr")
    @Expose
    private String zustadr;
    @SerializedName("strasse")
    @Expose
    private String strasse;
    @SerializedName("hausnr")
    @Expose
    private String hausnr;
    @SerializedName("adresszusatz")
    @Expose
    private String adresszusatz;
    @SerializedName("plz")
    @Expose
    private String plz;
    @SerializedName("ort")
    @Expose
    private String ort;
    @SerializedName("land_kennz")
    @Expose
    private String landKennz;
    @SerializedName("name1")
    @Expose
    private String name1;
    @SerializedName("name2")
    @Expose
    private String name2;
    @SerializedName("apartner_anrede_kennz")
    @Expose
    private String apartnerAnredeKennz;
    @SerializedName("apartner_vorname")
    @Expose
    private String apartnerVorname;
    @SerializedName("apartner_nachname")
    @Expose
    private String apartnerNachname;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;

    /**
     * 
     * @return
     *     The pk
     */
    public Integer getPk() {
        return pk;
    }

    /**
     * 
     * @param pk
     *     The pk
     */
    public void setPk(Integer pk) {
        this.pk = pk;
    }

    /**
     * 
     * @return
     *     The prio
     */
    public Integer getPrio() {
        return prio;
    }

    /**
     * 
     * @param prio
     *     The prio
     */
    public void setPrio(Integer prio) {
        this.prio = prio;
    }

    /**
     * 
     * @return
     *     The rechadr
     */
    public String getRechadr() {
        return rechadr;
    }

    /**
     * 
     * @param rechadr
     *     The rechadr
     */
    public void setRechadr(String rechadr) {
        this.rechadr = rechadr;
    }

    /**
     * 
     * @return
     *     The liefadr
     */
    public String getLiefadr() {
        return liefadr;
    }

    /**
     * 
     * @param liefadr
     *     The liefadr
     */
    public void setLiefadr(String liefadr) {
        this.liefadr = liefadr;
    }

    /**
     * 
     * @return
     *     The zustadr
     */
    public String getZustadr() {
        return zustadr;
    }

    /**
     * 
     * @param zustadr
     *     The zustadr
     */
    public void setZustadr(String zustadr) {
        this.zustadr = zustadr;
    }

    /**
     * 
     * @return
     *     The strasse
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * 
     * @param strasse
     *     The strasse
     */
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    /**
     * 
     * @return
     *     The hausnr
     */
    public String getHausnr() {
        return hausnr;
    }

    /**
     * 
     * @param hausnr
     *     The hausnr
     */
    public void setHausnr(String hausnr) {
        this.hausnr = hausnr;
    }

    /**
     * 
     * @return
     *     The adresszusatz
     */
    public String getAdresszusatz() {
        return adresszusatz;
    }

    /**
     * 
     * @param adresszusatz
     *     The adresszusatz
     */
    public void setAdresszusatz(String adresszusatz) {
        this.adresszusatz = adresszusatz;
    }

    /**
     * 
     * @return
     *     The plz
     */
    public String getPlz() {
        return plz;
    }

    /**
     * 
     * @param plz
     *     The plz
     */
    public void setPlz(String plz) {
        this.plz = plz;
    }

    /**
     * 
     * @return
     *     The ort
     */
    public String getOrt() {
        return ort;
    }

    /**
     * 
     * @param ort
     *     The ort
     */
    public void setOrt(String ort) {
        this.ort = ort;
    }

    /**
     * 
     * @return
     *     The landKennz
     */
    public String getLandKennz() {
        return landKennz;
    }

    /**
     * 
     * @param landKennz
     *     The land_kennz
     */
    public void setLandKennz(String landKennz) {
        this.landKennz = landKennz;
    }

    /**
     * 
     * @return
     *     The name1
     */
    public String getName1() {
        return name1;
    }

    /**
     * 
     * @param name1
     *     The name1
     */
    public void setName1(String name1) {
        this.name1 = name1;
    }

    /**
     * 
     * @return
     *     The name2
     */
    public String getName2() {
        return name2;
    }

    /**
     * 
     * @param name2
     *     The name2
     */
    public void setName2(String name2) {
        this.name2 = name2;
    }

    /**
     * 
     * @return
     *     The apartnerAnredeKennz
     */
    public String getApartnerAnredeKennz() {
        return apartnerAnredeKennz;
    }

    /**
     * 
     * @param apartnerAnredeKennz
     *     The apartner_anrede_kennz
     */
    public void setApartnerAnredeKennz(String apartnerAnredeKennz) {
        this.apartnerAnredeKennz = apartnerAnredeKennz;
    }

    /**
     * 
     * @return
     *     The apartnerVorname
     */
    public String getApartnerVorname() {
        return apartnerVorname;
    }

    /**
     * 
     * @param apartnerVorname
     *     The apartner_vorname
     */
    public void setApartnerVorname(String apartnerVorname) {
        this.apartnerVorname = apartnerVorname;
    }

    /**
     * 
     * @return
     *     The apartnerNachname
     */
    public String getApartnerNachname() {
        return apartnerNachname;
    }

    /**
     * 
     * @param apartnerNachname
     *     The apartner_nachname
     */
    public void setApartnerNachname(String apartnerNachname) {
        this.apartnerNachname = apartnerNachname;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
