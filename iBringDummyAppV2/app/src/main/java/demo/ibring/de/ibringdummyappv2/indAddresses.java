package demo.ibring.de.ibringdummyappv2;

import android.content.Context;
import android.content.DialogInterface;
import android.location.*;
import android.location.Address;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class indAddresses extends AppCompatActivity {
    Validator validator = new Validator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ind_addresses);
        final Validator validator = new Validator();
        List<String> addressList = new ArrayList<String>();


        AddressFounder addressFounder = new AddressFounder();
        List<android.location.Address> add = addressFounder.locationList(getApplicationContext(), "Breiter wrg 33 , magdeburg");
        for (int i = 0; i < add.size(); i++) {
            addressList.add(addressShow(i, add));
            String showAdd = addressList.get(i);
            showAdd = "";
        }
        final CharSequence address[] = addressList.toArray(new CharSequence[addressList.size()]);
        AlertDialog.Builder builder = new AlertDialog.Builder(indAddresses.this);
        builder.setTitle("did you mean");
        builder.setItems(address, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {



            }
        });
        builder.show();



       /* if (add.size()==1){
            if (!add.get(0).getAddressLine(0).equalsIgnoreCase("Breiter wrg 33")) {
                final CharSequence address[] = new CharSequence[]{add.get(0).getAddressLine(0) + " " + add.get(0).getAddressLine(1), "Breiter wrg 33 , magdeburg"};
                AlertDialog.Builder builder = new AlertDialog.Builder(indAddresses.this);
                builder.setTitle("did you mean");
                builder.setItems(address, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        validator.showErrorMessage("the User choosen " + address[which],getApplicationContext());

                    }
                });
                builder.show();
            }
        }else if (add.size() == 2){
            if (!add.get(0).getAddressLine(0).equalsIgnoreCase("Breiter wrg 33") || !add.get(1).getAddressLine(1).equalsIgnoreCase("Breiter wrg 33")) {
                final CharSequence address[] = new CharSequence[]{add.get(0).getAddressLine(0) + " " + add.get(0).getAddressLine(1), add.get(1).getAddressLine(0) + " " + add.get(1).getAddressLine(1) , "Breiter wrg 33 , magdeburg"};
                AlertDialog.Builder builder = new AlertDialog.Builder(indAddresses.this);
                builder.setTitle("did you mean");
                builder.setItems(address, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Toast.makeText(getApplicationContext(), "the user choosen " + address[which] + " " + which,Toast.LENGTH_LONG).show();

                    }
                });
                builder.show();
            }

        }*/
    }


    public String addressShow(int i, List<android.location.Address> addressList){
        String address;
        if (addressList != null){
            address = addressList.get(i).getAddressLine(0) + " " + addressList.get(i).getAddressLine(1);
            return address;
        }else {
            return "";
        }
    }

    public List<String> addressList(Context context, String address){
        AddressFounder addressFounder = new AddressFounder();
        List<String> addressList = new ArrayList<String>();
        List<android.location.Address> add = addressFounder.locationList(context, address);
        if (add != null) {
            for (int i = 0; i < add.size(); i++) {
                addressList.add(addressShow(i, add));
                String showAdd = addressList.get(i);
            }
            return  addressList;
        }
        return null;

    }



}
