package demo.ibring.de.ibringdummyappv2;

import android.content.Context;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by albert on 28/07/16.
 */
public class Loging_In_Volley {

    private  String URL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/v3/auth/login/";

    RequestQueue requestQueue;


    public void loginPost(Context context, JSONObject jsonObject, final String IP, final String clientInfo, final logInVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        }){@Override
            public Map<String, String> getHeaders() {

                Map<String, String>  params = new HashMap<String, String>();
                params.put("Remote-IP", IP);
                params.put("Client-Info", clientInfo);

                return params;

        }@Override
        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
            try {
                String jsonString = new String(response.data,
                        HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                JSONObject jj= new JSONObject(jsonString);
                String sessionToken = response.headers.get("Session-Token");
                jj.put("Session-Token", sessionToken);
                return Response.success(jj, HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException je) {
                return Response.error(new ParseError(je));
            }
        }
        };
        requestQueue.add(jsonObjectRequest);


    }



    public interface logInVolleyCallback {
        void onSuccess(String response);
        void onFail(VolleyError error);
    }
}
