package demo.ibring.de.backendstatus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class CustomerDataActivity extends AppCompatActivity {

    private AddressORM namefromdb = null;
    private AddressORM ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_data);

        Bundle bundle = getIntent().getExtras();
        final String pk = bundle.getString("Customer_pk");
        final String namestr = bundle.getString("Customer_Name");

        Button logout = (Button) findViewById(R.id.Logout_Button);
        logout.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerDataActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        //Intent i = getIntent();
        //Customer customer = (Customer) i.getSerializableExtra("Customer");

        /*int pkValue = new Integer(pk);
        try {
            ad.setPk(Integer.valueOf(pk));
        }catch(Exception e) {
            Log.d("Can not transform it to int", "");
        }


        List<AddressORM> list;
        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        try {
            Dao<AddressORM, Integer> addressORMs = databaseHelper.getDao();
            namefromdb = addressORMs.queryForId(pkValue);
            list = addressORMs.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        //String insert = namefromdb.getName1();

        EditText name = (EditText) findViewById(R.id.Name_EditText);
        name.setText(namestr);

        Button addressButton = (Button) findViewById(R.id.Addresse_Button);
        addressButton.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                Intent intent = new Intent(CustomerDataActivity.this, AddAddressActivity.class);
                intent.putExtra("pk", pk);
                startActivity(intent);
            }
        }) ;
    }
}
