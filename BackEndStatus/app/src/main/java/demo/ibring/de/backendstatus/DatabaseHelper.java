package demo.ibring.de.backendstatus;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by eliebitar on 27/07/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "customres";
    private static final int DATABASE_VERSION = 1;
    private final String TAG = "DBHelper";

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */
    private Dao<CustomersFinal.ItemsBean, Integer> itemsBeenORMs;
    private Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenORMs;
    private Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> PhoneNumbersBeanORMs;
    private Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> EmailAddressesBeanORMs;

    public DatabaseHelper(Context context) {
        super(
                context,
                DATABASE_NAME,
                null,
                DATABASE_VERSION,
                R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.class);
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.AddressesBean.class);
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.EmailAddressesBean.class);
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.PhoneNumbersBean.class);
        } catch (SQLException e) {
            Log.d(TAG,"Error creating the database " + DATABASE_NAME + ".");
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.class, false);
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.AddressesBean.class, false);
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.EmailAddressesBean.class, false);
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.PhoneNumbersBean.class, false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            Log.d(TAG,"Error Upgrading the database " + DATABASE_NAME + ".");
            e.printStackTrace();
        }
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<CustomersFinal.ItemsBean, Integer> getItemsBeanDao() throws SQLException {
        if(itemsBeenORMs == null) {
            itemsBeenORMs = getDao(CustomersFinal.ItemsBean.class);
        }
        return itemsBeenORMs;
    }

    public Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> getAddressesBeenDao() throws SQLException {
        if(addressesBeenORMs == null) {
            addressesBeenORMs = getDao(CustomersFinal.ItemsBean.AddressesBean.class);
        }
        return addressesBeenORMs;
    }

    public Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> getEmailAddressesBeanDao() throws SQLException {
        if(EmailAddressesBeanORMs == null) {
            EmailAddressesBeanORMs = getDao(CustomersFinal.ItemsBean.EmailAddressesBean.class);
        }
        return EmailAddressesBeanORMs;
    }

    public Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> getPhoneNumbersBeanDao() throws SQLException {
        if(PhoneNumbersBeanORMs == null) {
            PhoneNumbersBeanORMs = getDao(CustomersFinal.ItemsBean.PhoneNumbersBean.class);
        }
        return PhoneNumbersBeanORMs;
    }
}
