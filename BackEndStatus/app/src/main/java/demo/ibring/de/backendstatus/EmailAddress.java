
package demo.ibring.de.backendstatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmailAddress {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("standard")
    @Expose
    private String standard;
    @SerializedName("email")
    @Expose
    private String email;

    /**
     * 
     * @return
     *     The pk
     */
    public Integer getPk() {
        return pk;
    }

    /**
     * 
     * @param pk
     *     The pk
     */
    public void setPk(Integer pk) {
        this.pk = pk;
    }

    /**
     * 
     * @return
     *     The standard
     */
    public String getStandard() {
        return standard;
    }

    /**
     * 
     * @param standard
     *     The standard
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
