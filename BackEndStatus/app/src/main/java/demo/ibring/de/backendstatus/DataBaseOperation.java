package demo.ibring.de.backendstatus;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by eliebitar on 27/07/16.
 */
public class DataBaseOperation {
    String TAG = "DBop";
    DatabaseHelper databaseHelper;

    public DataBaseOperation(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    /**
     *
     * InsertOrUpdateTheTable IteamBean into the database..
     *
     * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean itemsBean) throws SQLException {

        Dao<CustomersFinal.ItemsBean, Integer> ItemsBeanDao = databaseHelper.getItemsBeanDao();



        Integer pk                     = itemsBean.getPk();
        String debitor                 = itemsBean.getDebitor();
        String klasse                  = itemsBean.getKlasse();
        String gast                    = itemsBean.getGast();
        String name1                   = itemsBean.getName1();
        String name2                   = itemsBean.getName2();
        String apartner_anrede_kennz   = itemsBean.getApartner_anrede_kennz();
        String apartner_vorname        = itemsBean.getApartner_vorname();
        String apartner_nachname       = itemsBean.getApartner_nachname();
        String geburtsdatum            = itemsBean.getGeburtsdatum();
        String zahlziel_kennz          = itemsBean.getZahlziel_kennz();
        String vertragsbeginn          = itemsBean.getVertragsbeginn();
        String ustandort_kennz         = itemsBean.getUstandort_kennz();
        String aktiv                   = itemsBean.getAktiv();

        try {
            ItemsBeanDao.createOrUpdate(new CustomersFinal.ItemsBean(
                    pk,
                    debitor,
                    klasse,
                    gast,
                    name1,
                    name2,
                    apartner_anrede_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    geburtsdatum,
                    zahlziel_kennz,
                    vertragsbeginn,
                    ustandort_kennz,
                    aktiv));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Update *****IteamBean****** into the table.......");
        }
        //List<CustomersFinal.ItemsBean> list = ItemsBeanDao.queryForAll();
    }

    /*
    *
    * InsertOrUpdateTheTable customer addresses into the database
    * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean.AddressesBean addressesBean) throws SQLException {

        Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenDao = databaseHelper.getAddressesBeenDao();


        Integer pk                   = addressesBean.getPk();
        Integer prio                 = addressesBean.getPrio();
        String rechadr               = addressesBean.getRechadr();
        String liefadr               = addressesBean.getLiefadr();
        String zustadr               = addressesBean.getZustadr();
        String strasse               = addressesBean.getStrasse();
        String hausnr                = addressesBean.getHausnr();
        String adresszusatz          = addressesBean.getAdresszusatz();
        String plz                   = addressesBean.getPlz();
        String ort                   = addressesBean.getOrt();
        String land_kennz            = addressesBean.getLand_kennz();
        String name1                 = addressesBean.getName1();
        String name2                 = addressesBean.getName2();
        String apartner_anrede_kennz = addressesBean.getApartner_anrede_kennz();
        String apartner_vorname      = addressesBean.getApartner_vorname();
        String apartner_nachname     = addressesBean.getApartner_nachname();
        double latitude              = addressesBean.getLatitude();
        double longitude             = addressesBean.getLongitude();


        try {
            addressesBeenDao.createOrUpdate(new CustomersFinal.ItemsBean.AddressesBean(
                    pk,
                    prio,
                    rechadr,
                    liefadr,
                    zustadr,
                    strasse,
                    hausnr,
                    adresszusatz,
                    plz,
                    ort,
                    land_kennz,
                    name1,
                    name2,
                    apartner_anrede_kennz,
                    apartner_vorname,
                    apartner_nachname,
                    latitude,
                    longitude));
        }catch (Exception e){
        Log.d(TAG, "Error Inserting or Updating ******AddressesBean****** into the table.......");
        }
    }

    /*
    *
    * InsertOrUpdateTheTable Customer email into the database..
    *
    * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean.EmailAddressesBean emailAddressesBean) throws SQLException {


        Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> emailAddressesBeanIntegerDao = databaseHelper.getEmailAddressesBeanDao();


        Integer pk      = emailAddressesBean.getPk();
        String standard = emailAddressesBean.getStandard();
        String email    = emailAddressesBean.getEmail();

        try {
            emailAddressesBeanIntegerDao.createOrUpdate(new CustomersFinal.ItemsBean.EmailAddressesBean(
                    pk,
                    standard,
                    email));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******EmailAddressesBean****** into the table.......");
        }

    }

    /*
    *
    * InsertOrUpdateTheTable customer phone number into the database..
    *
    * */

    public void InsertOrUpdateTheTable(CustomersFinal.ItemsBean.PhoneNumbersBean phoneNumbersBean) throws SQLException {

        Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> phoneNumbersBeanIntegerDao = databaseHelper.getPhoneNumbersBeanDao();



        Integer pk       = phoneNumbersBean.getPk();
        String typ       = phoneNumbersBean.getTyp();
        String standard  = phoneNumbersBean.getStandard();
        String rufnummer = phoneNumbersBean.getRufnummer();
        String bemerkung = phoneNumbersBean.getBemerkung();

        try {
            phoneNumbersBeanIntegerDao.createOrUpdate(new CustomersFinal.ItemsBean.PhoneNumbersBean(
                    pk,
                    typ,
                    standard,
                    rufnummer,
                    bemerkung));
        }catch (Exception e){
            Log.d(TAG, "Error Inserting or Updating ******PhoneNumbersBean****** into the table.......");
        }

    }

    /**
     *
     * Delete from table IteamBeans..
     *
     * */


    public boolean Delete(int ID, CustomersFinal.ItemsBean i) throws SQLException {
        Dao<CustomersFinal.ItemsBean, Integer> ItemsBeanDao = databaseHelper.getItemsBeanDao();
        try {
            ItemsBeanDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG,"Error Deleting ******ItemsBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table AddressesBean..
     *
     * */

    public boolean Delete(int ID, CustomersFinal.ItemsBean.AddressesBean a) throws SQLException {
        Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenDao = databaseHelper.getAddressesBeenDao();
        try {
            addressesBeenDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG,"Error Deleting ******AddressesBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table PhoneNumbersBean..
     *
     * */

    public boolean Delete(int ID, CustomersFinal.ItemsBean.PhoneNumbersBean p) throws SQLException {
        Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> phoneNumbersBeanIntegerDao = databaseHelper.getPhoneNumbersBeanDao();
        try {
            phoneNumbersBeanIntegerDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******PhoneNumbersBean****** record With the ID: " + ID + " .");
            return false;
        }
    }

    /**
     *
     * Delete from table EmailAddressesBean..
     *
     * */

    public boolean Delete(int ID, CustomersFinal.ItemsBean.EmailAddressesBean emailAddressesBean) throws SQLException {
        Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> emailAddressesBeanIntegerDao = databaseHelper.getEmailAddressesBeanDao();
        try {
            emailAddressesBeanIntegerDao.deleteById(ID);
            return true;
        }catch (Exception e){
            Log.d(TAG, "Error Deleting ******EmailAddressesBean****** record With the ID: " + ID + " .");
            return false;
        }
    }
}
