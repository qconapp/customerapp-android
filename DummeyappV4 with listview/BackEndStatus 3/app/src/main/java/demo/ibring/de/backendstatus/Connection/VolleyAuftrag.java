package demo.ibring.de.backendstatus.Connection;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by albert on 08/08/16.
 */
public class VolleyAuftrag {
    RequestQueue requestQueue;


    public void postAuftrag(Context context, JSONObject jsonObject, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);
        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);


    }

    public void postOrderItem(Context context, JSONObject jsonObject,String order_pk, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);
        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/"+order_pk+"/order_items";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);


    }

    public void getAuftrag(Context context, String order_pk, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);
        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/"+order_pk+"/";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }
    public void getAllOrederItems(Context context, String order_pk, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);
        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/"+order_pk+"/order_items/";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void getOneOrederItems(Context context, String order_pk, String order_Item_pk, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);
        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/"+order_pk+"/order_items/"+order_Item_pk+"/";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void deleteAAuftrag(Context context, String order_pk, final AddressesVolleyCallback callback){

        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/"+order_pk+"/";
        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void deleteOneOrderItem(Context context, String order_pk, String order_Item_pk, final AddressesVolleyCallback callback){

        String url = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/orders/"+order_pk+"/order_items/"+order_Item_pk+"/";
        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void putAuftrag(Context context, String auftrag_pk, JSONObject jsonObject, final AddressesVolleyCallback callback){

        String URL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/customers/"+auftrag_pk+"/";


        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onFail(error);

            }
        });

        requestQueue.add(jsonObjectRequest);
    }
    public void putOneOrderItem(Context context, String auftrag_pk,String order_Item_pk,  JSONObject jsonObject, final AddressesVolleyCallback callback){

        String URL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/customers/"+auftrag_pk+"/order_items/"+order_Item_pk+"/";


        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onFail(error);

            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    

    public interface AddressesVolleyCallback{
        void onSuccess(String response);
        void onFail(VolleyError error);
    }
}
