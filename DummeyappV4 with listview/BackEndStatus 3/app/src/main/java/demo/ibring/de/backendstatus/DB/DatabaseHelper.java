package demo.ibring.de.backendstatus.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import demo.ibring.de.backendstatus.CustomersFinal;
import demo.ibring.de.backendstatus.Orders;
import demo.ibring.de.backendstatus.R;

/**
 * Created by eliebitar on 27/07/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "customres";
    private static final int DATABASE_VERSION = 1;
    private final String TAG = "DBHelper";

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */
    private Dao<CustomersFinal.ItemsBean, Integer> itemsBeenORMs;
    private Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenORMs;
    private Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> PhoneNumbersBeanORMs;
    private Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> EmailAddressesBeanORMs;
    private Dao<Orders.ItemsBean, Integer> iteamsBeanOrdersORMs;
    private Dao<Orders.ItemsBean.AbholadrBean, Integer> abholadrBeanORMs;
    private Dao<Orders.ItemsBean.AbholpartnerBean, Integer> abholpartnerBeanORMs;
    private Dao<Orders.ItemsBean.AuftraggeberBean, Integer> auftraggeberBeanORMs;
    private Dao<Orders.ItemsBean.AuftragsstatusObjBean, Integer> auftragsstatusObjBeanORMs;
    private Dao<Orders.ItemsBean.OrderStatusLogBean, Integer> orderStatusLogBeanORMs;
    private Dao<Orders.ItemsBean.ZustelladrBean, Integer> zustelladrBeanORMs;
    private Dao<Orders.ItemsBean.ZustellpartnerBean, Integer> zustellpartnerBeanORMs;
    private Dao<Orders.ItemsBean.OrderItemsBean, Integer> OrderItemsBeanORMs;

    public DatabaseHelper(Context context) {
        super(
                context,
                DATABASE_NAME,
                null,
                DATABASE_VERSION,
                R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.class);
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.AddressesBean.class);
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.EmailAddressesBean.class);
            TableUtils.createTable(connectionSource, CustomersFinal.ItemsBean.PhoneNumbersBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.AbholadrBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.AbholpartnerBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.AuftraggeberBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.AuftragsstatusObjBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.OrderStatusLogBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.ZustelladrBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.ZustellpartnerBean.class);
            TableUtils.createTable(connectionSource, Orders.ItemsBean.OrderItemsBean.class);
        } catch (SQLException e) {
            Log.d(TAG,"Error creating the database " + DATABASE_NAME + ".");
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.class,                    false);
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.AddressesBean.class,      false);
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.EmailAddressesBean.class, false);
            TableUtils.dropTable(connectionSource, CustomersFinal.ItemsBean.PhoneNumbersBean.class,   false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.class,                            false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.AbholadrBean.class,               false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.AbholpartnerBean.class,           false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.AuftraggeberBean.class,           false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.AuftragsstatusObjBean.class,      false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.OrderStatusLogBean.class,         false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.ZustelladrBean.class,             false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.ZustellpartnerBean.class,         false);
            TableUtils.dropTable(connectionSource, Orders.ItemsBean.OrderItemsBean.class,             false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            Log.d(TAG,"Error Upgrading the database " + DATABASE_NAME + ".");
            e.printStackTrace();
        }
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<CustomersFinal.ItemsBean, Integer> getItemsBeanDao() throws SQLException {
        if(itemsBeenORMs == null) {
            itemsBeenORMs = getDao(CustomersFinal.ItemsBean.class);
        }
        return itemsBeenORMs;
    }

    public Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> getAddressesBeenDao() throws SQLException {
        if(addressesBeenORMs == null) {
            addressesBeenORMs = getDao(CustomersFinal.ItemsBean.AddressesBean.class);
        }
        return addressesBeenORMs;
    }

    public Dao<CustomersFinal.ItemsBean.EmailAddressesBean, Integer> getEmailAddressesBeanDao() throws SQLException {
        if(EmailAddressesBeanORMs == null) {
            EmailAddressesBeanORMs = getDao(CustomersFinal.ItemsBean.EmailAddressesBean.class);
        }
        return EmailAddressesBeanORMs;
    }

    public Dao<CustomersFinal.ItemsBean.PhoneNumbersBean, Integer> getPhoneNumbersBeanDao() throws SQLException {
        if(PhoneNumbersBeanORMs == null) {
            PhoneNumbersBeanORMs = getDao(CustomersFinal.ItemsBean.PhoneNumbersBean.class);
        }
        return PhoneNumbersBeanORMs;
    }

    public Dao<Orders.ItemsBean, Integer> getItemsBeanOrdersDao() throws SQLException {
        if(iteamsBeanOrdersORMs == null) {
            iteamsBeanOrdersORMs = getDao(Orders.ItemsBean.class);
        }
        return iteamsBeanOrdersORMs;
    }

    public Dao<Orders.ItemsBean.AbholadrBean, Integer> getAbholadrBeanDao() throws SQLException {
        if(abholadrBeanORMs == null) {
            abholadrBeanORMs = getDao(Orders.ItemsBean.AbholadrBean.class);
        }
        return abholadrBeanORMs;
    }

    public Dao<Orders.ItemsBean.AbholpartnerBean, Integer> getAbholpartnerBeanDao() throws SQLException {
        if(abholpartnerBeanORMs == null) {
            abholpartnerBeanORMs = getDao(Orders.ItemsBean.AbholpartnerBean.class);
        }
        return abholpartnerBeanORMs;
    }

    public Dao<Orders.ItemsBean.AuftraggeberBean, Integer> getAuftraggeberBeanDao() throws SQLException {
        if(auftraggeberBeanORMs == null) {
            auftraggeberBeanORMs = getDao(Orders.ItemsBean.AuftraggeberBean.class);
        }
        return auftraggeberBeanORMs;
    }

    public Dao<Orders.ItemsBean.AuftragsstatusObjBean, Integer> getAuftragsstatusObjBeanDao() throws SQLException {
        if(auftragsstatusObjBeanORMs == null) {
            auftragsstatusObjBeanORMs = getDao(Orders.ItemsBean.AuftragsstatusObjBean.class);
        }
        return auftragsstatusObjBeanORMs;
    }

    public Dao<Orders.ItemsBean.OrderStatusLogBean, Integer> getOrderStatusLogBeanDao() throws SQLException {
        if(orderStatusLogBeanORMs == null) {
            orderStatusLogBeanORMs = getDao(Orders.ItemsBean.OrderStatusLogBean.class);
        }
        return orderStatusLogBeanORMs;
    }

    public Dao<Orders.ItemsBean.ZustelladrBean, Integer> getZustelladrBeanDao() throws SQLException {
        if(zustelladrBeanORMs == null) {
            zustelladrBeanORMs = getDao(Orders.ItemsBean.ZustelladrBean.class);
        }
        return zustelladrBeanORMs;
    }

    public Dao<Orders.ItemsBean.ZustellpartnerBean, Integer> getZustellpartnerBeanDao() throws SQLException {
        if(zustellpartnerBeanORMs == null) {
            zustellpartnerBeanORMs = getDao(Orders.ItemsBean.ZustellpartnerBean.class);
        }
        return zustellpartnerBeanORMs;
    }

    public Dao<Orders.ItemsBean.OrderItemsBean, Integer> getOrderItemsBeanDao() throws SQLException {
        if(OrderItemsBeanORMs == null) {
            OrderItemsBeanORMs = getDao(Orders.ItemsBean.OrderItemsBean.class);
        }
        return OrderItemsBeanORMs;
    }
}
