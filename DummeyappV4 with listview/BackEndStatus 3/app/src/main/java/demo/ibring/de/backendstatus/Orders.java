package demo.ibring.de.backendstatus;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by eliebitar on 08/08/16.
 */
public class Orders {


    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    @DatabaseTable(tableName = "ItemsBean_Orders")
    public static class ItemsBean {

        @DatabaseField(id = true)
        private int pk;

        @DatabaseField
        private int auftragsnummer;

        @DatabaseField
        private int auftraggeber_fk;

        @DatabaseField
        private int abholpartner_fk;

        private AbholpartnerBean abholpartner;

        @DatabaseField
        private int abholadr_fk;

        @DatabaseField
        private int zustellpartner_fk;

        @DatabaseField
        private int zustelladr_fk;

        private ZustelladrBean zustelladr;

        @DatabaseField
        private String ustandort_kennz;

        @DatabaseField
        private String apartner_anrede_kennz;

        @DatabaseField
        private String apartner_vorname;

        @DatabaseField
        private String apartner_nachname;

        @DatabaseField
        private String abholen_von;

        @DatabaseField
        private String abholen_bis;

        @DatabaseField
        private String abholen_wunsch;

        @DatabaseField
        private String zustellen_von;

        @DatabaseField
        private String zustellen_bis;

        @DatabaseField
        private String zustellen_wunsch;

        @DatabaseField
        private String ibring_spezial_aadr;

        @DatabaseField
        private String ibring_spezial_zadr;

        @DatabaseField
        private String ibring_typ;

        @DatabaseField
        private String sammel_hash;

        @DatabaseField
        private String auftragsstatus;

        @DatabaseField
        private String auftrag_erledigt_am;

        private AuftragsstatusObjBean auftragsstatus_obj;
        private List<OrderItemsBean> order_items;
        private AuftraggeberBean auftraggeber;
        private Object rechempf_fk;
        private ZustellpartnerBean zustellpartner;
        private AbholadrBean abholadr;

        public ItemsBean() {
        }

        public ItemsBean(
                int pk,
                int auftragsnummer,
                int auftraggeber_fk,
                int abholpartner_fk,
                int abholadr_fk,
                int zustellpartner_fk,
                int zustelladr_fk,
                String ustandort_kennz,
                String apartner_anrede_kennz,
                String apartner_vorname,
                String apartner_nachname,
                String abholen_von,
                String abholen_bis,
                String abholen_wunsch,
                String zustellen_von,
                String zustellen_bis,
                String zustellen_wunsch,
                String ibring_spezial_aadr,
                String ibring_spezial_zadr,
                String ibring_typ,
                String sammel_hash,
                String auftragsstatus,
                String auftrag_erledigt_am) {
            this.pk = pk;
            this.auftragsnummer = auftragsnummer;
            this.auftraggeber_fk = auftraggeber_fk;
            this.abholpartner_fk = abholpartner_fk;
            this.abholadr_fk = abholadr_fk;
            this.zustellpartner_fk = zustellpartner_fk;
            this.zustelladr_fk = zustelladr_fk;
            this.ustandort_kennz = ustandort_kennz;
            this.apartner_anrede_kennz = apartner_anrede_kennz;
            this.apartner_vorname = apartner_vorname;
            this.apartner_nachname = apartner_nachname;
            this.abholen_von = abholen_von;
            this.abholen_bis = abholen_bis;
            this.abholen_wunsch = abholen_wunsch;
            this.zustellen_von = zustellen_von;
            this.zustellen_bis = zustellen_bis;
            this.zustellen_wunsch = zustellen_wunsch;
            this.ibring_spezial_aadr = ibring_spezial_aadr;
            this.ibring_spezial_zadr = ibring_spezial_zadr;
            this.ibring_typ = ibring_typ;
            this.sammel_hash = sammel_hash;
            this.auftragsstatus = auftragsstatus;
            this.auftrag_erledigt_am = auftrag_erledigt_am;
        }








        private List<OrderStatusLogBean> order_status_log;

        public int getPk() {
            return pk;
        }

        public void setPk(int pk) {
            this.pk = pk;
        }

        public int getAuftragsnummer() {
            return auftragsnummer;
        }

        public void setAuftragsnummer(int auftragsnummer) {
            this.auftragsnummer = auftragsnummer;
        }

        public int getAuftraggeber_fk() {
            return auftraggeber_fk;
        }

        public void setAuftraggeber_fk(int auftraggeber_fk) {
            this.auftraggeber_fk = auftraggeber_fk;
        }

        public AuftraggeberBean getAuftraggeber() {
            return auftraggeber;
        }

        public void setAuftraggeber(AuftraggeberBean auftraggeber) {
            this.auftraggeber = auftraggeber;
        }

        public Object getRechempf_fk() {
            return rechempf_fk;
        }

        public void setRechempf_fk(Object rechempf_fk) {
            this.rechempf_fk = rechempf_fk;
        }

        public int getAbholpartner_fk() {
            return abholpartner_fk;
        }

        public void setAbholpartner_fk(int abholpartner_fk) {
            this.abholpartner_fk = abholpartner_fk;
        }

        public AbholpartnerBean getAbholpartner() {
            return abholpartner;
        }

        public void setAbholpartner(AbholpartnerBean abholpartner) {
            this.abholpartner = abholpartner;
        }

        public int getAbholadr_fk() {
            return abholadr_fk;
        }

        public void setAbholadr_fk(int abholadr_fk) {
            this.abholadr_fk = abholadr_fk;
        }

        public AbholadrBean getAbholadr() {
            return abholadr;
        }

        public void setAbholadr(AbholadrBean abholadr) {
            this.abholadr = abholadr;
        }

        public int getZustellpartner_fk() {
            return zustellpartner_fk;
        }

        public void setZustellpartner_fk(int zustellpartner_fk) {
            this.zustellpartner_fk = zustellpartner_fk;
        }

        public ZustellpartnerBean getZustellpartner() {
            return zustellpartner;
        }

        public void setZustellpartner(ZustellpartnerBean zustellpartner) {
            this.zustellpartner = zustellpartner;
        }

        public int getZustelladr_fk() {
            return zustelladr_fk;
        }

        public void setZustelladr_fk(int zustelladr_fk) {
            this.zustelladr_fk = zustelladr_fk;
        }

        public ZustelladrBean getZustelladr() {
            return zustelladr;
        }

        public void setZustelladr(ZustelladrBean zustelladr) {
            this.zustelladr = zustelladr;
        }

        public String getUstandort_kennz() {
            return ustandort_kennz;
        }

        public void setUstandort_kennz(String ustandort_kennz) {
            this.ustandort_kennz = ustandort_kennz;
        }

        public String getApartner_anrede_kennz() {
            return apartner_anrede_kennz;
        }

        public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
            this.apartner_anrede_kennz = apartner_anrede_kennz;
        }

        public String getApartner_vorname() {
            return apartner_vorname;
        }

        public void setApartner_vorname(String apartner_vorname) {
            this.apartner_vorname = apartner_vorname;
        }

        public String getApartner_nachname() {
            return apartner_nachname;
        }

        public void setApartner_nachname(String apartner_nachname) {
            this.apartner_nachname = apartner_nachname;
        }

        public String getAbholen_von() {
            return abholen_von;
        }

        public void setAbholen_von(String abholen_von) {
            this.abholen_von = abholen_von;
        }

        public String getAbholen_bis() {
            return abholen_bis;
        }

        public void setAbholen_bis(String abholen_bis) {
            this.abholen_bis = abholen_bis;
        }

        public String getAbholen_wunsch() {
            return abholen_wunsch;
        }

        public void setAbholen_wunsch(String abholen_wunsch) {
            this.abholen_wunsch = abholen_wunsch;
        }

        public String getZustellen_von() {
            return zustellen_von;
        }

        public void setZustellen_von(String zustellen_von) {
            this.zustellen_von = zustellen_von;
        }

        public String getZustellen_bis() {
            return zustellen_bis;
        }

        public void setZustellen_bis(String zustellen_bis) {
            this.zustellen_bis = zustellen_bis;
        }

        public String getZustellen_wunsch() {
            return zustellen_wunsch;
        }

        public void setZustellen_wunsch(String zustellen_wunsch) {
            this.zustellen_wunsch = zustellen_wunsch;
        }

        public String getIbring_spezial_aadr() {
            return ibring_spezial_aadr;
        }

        public void setIbring_spezial_aadr(String ibring_spezial_aadr) {
            this.ibring_spezial_aadr = ibring_spezial_aadr;
        }

        public String getIbring_spezial_zadr() {
            return ibring_spezial_zadr;
        }

        public void setIbring_spezial_zadr(String ibring_spezial_zadr) {
            this.ibring_spezial_zadr = ibring_spezial_zadr;
        }

        public String getIbring_typ() {
            return ibring_typ;
        }

        public void setIbring_typ(String ibring_typ) {
            this.ibring_typ = ibring_typ;
        }

        public String getSammel_hash() {
            return sammel_hash;
        }

        public void setSammel_hash(String sammel_hash) {
            this.sammel_hash = sammel_hash;
        }

        public String getAuftragsstatus() {
            return auftragsstatus;
        }

        public void setAuftragsstatus(String auftragsstatus) {
            this.auftragsstatus = auftragsstatus;
        }

        public String getAuftrag_erledigt_am() {
            return auftrag_erledigt_am;
        }

        public void setAuftrag_erledigt_am(String auftrag_erledigt_am) {
            this.auftrag_erledigt_am = auftrag_erledigt_am;
        }

        public AuftragsstatusObjBean getAuftragsstatus_obj() {
            return auftragsstatus_obj;
        }

        public void setAuftragsstatus_obj(AuftragsstatusObjBean auftragsstatus_obj) {
            this.auftragsstatus_obj = auftragsstatus_obj;
        }

        public List<OrderItemsBean> getOrder_items() {
            return order_items;
        }

        public void setOrder_items(List<OrderItemsBean> order_items) {
            this.order_items = order_items;
        }

        public List<OrderStatusLogBean> getOrder_status_log() {
            return order_status_log;
        }

        public void setOrder_status_log(List<OrderStatusLogBean> order_status_log) {
            this.order_status_log = order_status_log;
        }

        @DatabaseTable(tableName = "AuftraggeberBean")
        public static class AuftraggeberBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private String debitor;

            @DatabaseField
            private String klasse;

            @DatabaseField
            private String ibring_gast;

            public AuftraggeberBean(Integer ID,
                                    int pk,
                                    String debitor,
                                    String klasse,
                                    String ibring_gast,
                                    String name1,
                                    String name2,
                                    String apartner_anrede_kennz,
                                    String apartner_vorname,
                                    String apartner_nachname,
                                    String ustandort_kennz) {
                this.ID = ID;
                this.pk = pk;
                this.debitor = debitor;
                this.klasse = klasse;
                this.ibring_gast = ibring_gast;
                this.name1 = name1;
                this.name2 = name2;
                this.apartner_anrede_kennz = apartner_anrede_kennz;
                this.apartner_vorname = apartner_vorname;
                this.apartner_nachname = apartner_nachname;
                this.ustandort_kennz = ustandort_kennz;
            }

            public AuftraggeberBean() {
            }

            @DatabaseField
            private String name1;

            @DatabaseField
            private String name2;

            @DatabaseField
            private String apartner_anrede_kennz;

            @DatabaseField
            private String apartner_vorname;

            @DatabaseField
            private String apartner_nachname;

            @DatabaseField
            private String ustandort_kennz;

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getDebitor() {
                return debitor;
            }

            public void setDebitor(String debitor) {
                this.debitor = debitor;
            }

            public String getKlasse() {
                return klasse;
            }

            public void setKlasse(String klasse) {
                this.klasse = klasse;
            }

            public String getIbring_gast() {
                return ibring_gast;
            }

            public void setIbring_gast(String ibring_gast) {
                this.ibring_gast = ibring_gast;
            }

            public String getName1() {
                return name1;
            }

            public void setName1(String name1) {
                this.name1 = name1;
            }

            public String getName2() {
                return name2;
            }

            public void setName2(String name2) {
                this.name2 = name2;
            }

            public String getApartner_anrede_kennz() {
                return apartner_anrede_kennz;
            }

            public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
                this.apartner_anrede_kennz = apartner_anrede_kennz;
            }

            public String getApartner_vorname() {
                return apartner_vorname;
            }

            public void setApartner_vorname(String apartner_vorname) {
                this.apartner_vorname = apartner_vorname;
            }

            public String getApartner_nachname() {
                return apartner_nachname;
            }

            public void setApartner_nachname(String apartner_nachname) {
                this.apartner_nachname = apartner_nachname;
            }

            public String getUstandort_kennz() {
                return ustandort_kennz;
            }

            public void setUstandort_kennz(String ustandort_kennz) {
                this.ustandort_kennz = ustandort_kennz;
            }
        }

        @DatabaseTable(tableName = "AbholpartnerBean")
        public static class AbholpartnerBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private String debitor;

            @DatabaseField
            private String klasse;

            @DatabaseField
            private String ibring_gast;

            @DatabaseField
            private String name1;

            @DatabaseField
            private String name2;

            @DatabaseField
            private String apartner_anrede_kennz;

            @DatabaseField
            private String apartner_vorname;

            @DatabaseField
            private String apartner_nachname;

            @DatabaseField
            private String ustandort_kennz;

            public AbholpartnerBean() {
            }

            public AbholpartnerBean(String ustandort_kennz,
                                    String apartner_vorname,
                                    String apartner_nachname,
                                    String apartner_anrede_kennz,
                                    String name2, String name1,
                                    String ibring_gast,
                                    String klasse,
                                    String debitor,
                                    int pk,
                                    Integer ID) {
                this.ustandort_kennz = ustandort_kennz;
                this.apartner_vorname = apartner_vorname;
                this.apartner_nachname = apartner_nachname;
                this.apartner_anrede_kennz = apartner_anrede_kennz;
                this.name2 = name2;
                this.name1 = name1;
                this.ibring_gast = ibring_gast;
                this.klasse = klasse;
                this.debitor = debitor;
                this.pk = pk;
                this.ID = ID;
            }

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getDebitor() {
                return debitor;
            }

            public void setDebitor(String debitor) {
                this.debitor = debitor;
            }

            public String getKlasse() {
                return klasse;
            }

            public void setKlasse(String klasse) {
                this.klasse = klasse;
            }

            public String getIbring_gast() {
                return ibring_gast;
            }

            public void setIbring_gast(String ibring_gast) {
                this.ibring_gast = ibring_gast;
            }

            public String getName1() {
                return name1;
            }

            public void setName1(String name1) {
                this.name1 = name1;
            }

            public String getName2() {
                return name2;
            }

            public void setName2(String name2) {
                this.name2 = name2;
            }

            public String getApartner_anrede_kennz() {
                return apartner_anrede_kennz;
            }

            public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
                this.apartner_anrede_kennz = apartner_anrede_kennz;
            }

            public String getApartner_vorname() {
                return apartner_vorname;
            }

            public void setApartner_vorname(String apartner_vorname) {
                this.apartner_vorname = apartner_vorname;
            }

            public String getApartner_nachname() {
                return apartner_nachname;
            }

            public void setApartner_nachname(String apartner_nachname) {
                this.apartner_nachname = apartner_nachname;
            }

            public String getUstandort_kennz() {
                return ustandort_kennz;
            }

            public void setUstandort_kennz(String ustandort_kennz) {
                this.ustandort_kennz = ustandort_kennz;
            }
        }

        @DatabaseTable(tableName = "AbholadrBean")
        public static class AbholadrBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private String strasse;

            @DatabaseField
            private String hausnr;

            @DatabaseField
            private String plz;

            @DatabaseField
            private String ort;

            @DatabaseField
            private String adresszusatz;

            @DatabaseField
            private String land_kennz;

            @DatabaseField
            private String is_delivery_area;

            @DatabaseField
            private String ustandort_kennz;

            @DatabaseField
            private String apartner_name1;

            @DatabaseField
            private String apartner_name2;

            @DatabaseField
            private String apartner_anrede_kennz;

            @DatabaseField
            private String apartner_vorname;

            @DatabaseField
            private String apartner_nachname;

            public AbholadrBean() {
            }

            public AbholadrBean(String apartner_nachname,
                                String apartner_vorname,
                                String apartner_anrede_kennz,
                                String apartner_name2,
                                String apartner_name1,
                                String ustandort_kennz,
                                String is_delivery_area,
                                String land_kennz,
                                String adresszusatz,
                                String ort, String plz,
                                String hausnr,
                                String strasse,
                                int pk,
                                Integer ID) {
                this.apartner_nachname = apartner_nachname;
                this.apartner_vorname = apartner_vorname;
                this.apartner_anrede_kennz = apartner_anrede_kennz;
                this.apartner_name2 = apartner_name2;
                this.apartner_name1 = apartner_name1;
                this.ustandort_kennz = ustandort_kennz;
                this.is_delivery_area = is_delivery_area;
                this.land_kennz = land_kennz;
                this.adresszusatz = adresszusatz;
                this.ort = ort;
                this.plz = plz;
                this.hausnr = hausnr;
                this.strasse = strasse;
                this.pk = pk;
                this.ID = ID;
            }

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getStrasse() {
                return strasse;
            }

            public void setStrasse(String strasse) {
                this.strasse = strasse;
            }

            public String getHausnr() {
                return hausnr;
            }

            public void setHausnr(String hausnr) {
                this.hausnr = hausnr;
            }

            public String getPlz() {
                return plz;
            }

            public void setPlz(String plz) {
                this.plz = plz;
            }

            public String getOrt() {
                return ort;
            }

            public void setOrt(String ort) {
                this.ort = ort;
            }

            public String getAdresszusatz() {
                return adresszusatz;
            }

            public void setAdresszusatz(String adresszusatz) {
                this.adresszusatz = adresszusatz;
            }

            public String getLand_kennz() {
                return land_kennz;
            }

            public void setLand_kennz(String land_kennz) {
                this.land_kennz = land_kennz;
            }

            public String getIs_delivery_area() {
                return is_delivery_area;
            }

            public void setIs_delivery_area(String is_delivery_area) {
                this.is_delivery_area = is_delivery_area;
            }

            public String getUstandort_kennz() {
                return ustandort_kennz;
            }

            public void setUstandort_kennz(String ustandort_kennz) {
                this.ustandort_kennz = ustandort_kennz;
            }

            public String getApartner_name1() {
                return apartner_name1;
            }

            public void setApartner_name1(String apartner_name1) {
                this.apartner_name1 = apartner_name1;
            }

            public String getApartner_name2() {
                return apartner_name2;
            }

            public void setApartner_name2(String apartner_name2) {
                this.apartner_name2 = apartner_name2;
            }

            public String getApartner_anrede_kennz() {
                return apartner_anrede_kennz;
            }

            public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
                this.apartner_anrede_kennz = apartner_anrede_kennz;
            }

            public String getApartner_vorname() {
                return apartner_vorname;
            }

            public void setApartner_vorname(String apartner_vorname) {
                this.apartner_vorname = apartner_vorname;
            }

            public String getApartner_nachname() {
                return apartner_nachname;
            }

            public void setApartner_nachname(String apartner_nachname) {
                this.apartner_nachname = apartner_nachname;
            }
        }

        @DatabaseTable(tableName = "ZustellpartnerBean")
        public static class ZustellpartnerBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private String debitor;

            @DatabaseField
            private String klasse;

            @DatabaseField
            private String ibring_gast;

            @DatabaseField
            private String name1;

            @DatabaseField
            private String name2;

            @DatabaseField
            private String apartner_anrede_kennz;

            @DatabaseField
            private String apartner_vorname;

            @DatabaseField
            private String apartner_nachname;

            @DatabaseField
            private String ustandort_kennz;

            public ZustellpartnerBean() {
            }

            public ZustellpartnerBean(String ustandort_kennz,
                                      String apartner_nachname,
                                      String apartner_vorname,
                                      String apartner_anrede_kennz,
                                      String name2,
                                      String name1,
                                      String ibring_gast,
                                      String klasse,
                                      String debitor,
                                      int pk,
                                      Integer ID) {
                this.ustandort_kennz = ustandort_kennz;
                this.apartner_nachname = apartner_nachname;
                this.apartner_vorname = apartner_vorname;
                this.apartner_anrede_kennz = apartner_anrede_kennz;
                this.name2 = name2;
                this.name1 = name1;
                this.ibring_gast = ibring_gast;
                this.klasse = klasse;
                this.debitor = debitor;
                this.pk = pk;
                this.ID = ID;
            }

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getDebitor() {
                return debitor;
            }

            public void setDebitor(String debitor) {
                this.debitor = debitor;
            }

            public String getKlasse() {
                return klasse;
            }

            public void setKlasse(String klasse) {
                this.klasse = klasse;
            }

            public String getIbring_gast() {
                return ibring_gast;
            }

            public void setIbring_gast(String ibring_gast) {
                this.ibring_gast = ibring_gast;
            }

            public String getName1() {
                return name1;
            }

            public void setName1(String name1) {
                this.name1 = name1;
            }

            public String getName2() {
                return name2;
            }

            public void setName2(String name2) {
                this.name2 = name2;
            }

            public String getApartner_anrede_kennz() {
                return apartner_anrede_kennz;
            }

            public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
                this.apartner_anrede_kennz = apartner_anrede_kennz;
            }

            public String getApartner_vorname() {
                return apartner_vorname;
            }

            public void setApartner_vorname(String apartner_vorname) {
                this.apartner_vorname = apartner_vorname;
            }

            public String getApartner_nachname() {
                return apartner_nachname;
            }

            public void setApartner_nachname(String apartner_nachname) {
                this.apartner_nachname = apartner_nachname;
            }

            public String getUstandort_kennz() {
                return ustandort_kennz;
            }

            public void setUstandort_kennz(String ustandort_kennz) {
                this.ustandort_kennz = ustandort_kennz;
            }
        }

        @DatabaseTable(tableName = "ZustelladrBean")
        public static class ZustelladrBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private String strasse;

            @DatabaseField
            private String hausnr;

            @DatabaseField
            private String plz;

            @DatabaseField
            private String ort;

            @DatabaseField
            private String adresszusatz;

            @DatabaseField
            private String land_kennz;

            @DatabaseField
            private String is_delivery_area;

            @DatabaseField
            private String ustandort_kennz;

            @DatabaseField
            private String apartner_name1;

            @DatabaseField
            private String apartner_name2;

            @DatabaseField
            private String apartner_anrede_kennz;

            @DatabaseField
            private String apartner_vorname;

            @DatabaseField
            private String apartner_nachname;

            public ZustelladrBean() {
            }

            public ZustelladrBean(Integer ID,
                                  String apartner_nachname,
                                  String apartner_vorname,
                                  String apartner_anrede_kennz,
                                  String apartner_name2,
                                  String apartner_name1,
                                  String ustandort_kennz,
                                  String is_delivery_area,
                                  String land_kennz,
                                  String adresszusatz,
                                  String ort,
                                  String plz,
                                  String hausnr,
                                  String strasse,
                                  int pk) {
                this.ID = ID;
                this.apartner_nachname = apartner_nachname;
                this.apartner_vorname = apartner_vorname;
                this.apartner_anrede_kennz = apartner_anrede_kennz;
                this.apartner_name2 = apartner_name2;
                this.apartner_name1 = apartner_name1;
                this.ustandort_kennz = ustandort_kennz;
                this.is_delivery_area = is_delivery_area;
                this.land_kennz = land_kennz;
                this.adresszusatz = adresszusatz;
                this.ort = ort;
                this.plz = plz;
                this.hausnr = hausnr;
                this.strasse = strasse;
                this.pk = pk;
            }

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getStrasse() {
                return strasse;
            }

            public void setStrasse(String strasse) {
                this.strasse = strasse;
            }

            public String getHausnr() {
                return hausnr;
            }

            public void setHausnr(String hausnr) {
                this.hausnr = hausnr;
            }

            public String getPlz() {
                return plz;
            }

            public void setPlz(String plz) {
                this.plz = plz;
            }

            public String getOrt() {
                return ort;
            }

            public void setOrt(String ort) {
                this.ort = ort;
            }

            public String getAdresszusatz() {
                return adresszusatz;
            }

            public void setAdresszusatz(String adresszusatz) {
                this.adresszusatz = adresszusatz;
            }

            public String getLand_kennz() {
                return land_kennz;
            }

            public void setLand_kennz(String land_kennz) {
                this.land_kennz = land_kennz;
            }

            public String getIs_delivery_area() {
                return is_delivery_area;
            }

            public void setIs_delivery_area(String is_delivery_area) {
                this.is_delivery_area = is_delivery_area;
            }

            public String getUstandort_kennz() {
                return ustandort_kennz;
            }

            public void setUstandort_kennz(String ustandort_kennz) {
                this.ustandort_kennz = ustandort_kennz;
            }

            public String getApartner_name1() {
                return apartner_name1;
            }

            public void setApartner_name1(String apartner_name1) {
                this.apartner_name1 = apartner_name1;
            }

            public String getApartner_name2() {
                return apartner_name2;
            }

            public void setApartner_name2(String apartner_name2) {
                this.apartner_name2 = apartner_name2;
            }

            public String getApartner_anrede_kennz() {
                return apartner_anrede_kennz;
            }

            public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
                this.apartner_anrede_kennz = apartner_anrede_kennz;
            }

            public String getApartner_vorname() {
                return apartner_vorname;
            }

            public void setApartner_vorname(String apartner_vorname) {
                this.apartner_vorname = apartner_vorname;
            }

            public String getApartner_nachname() {
                return apartner_nachname;
            }

            public void setApartner_nachname(String apartner_nachname) {
                this.apartner_nachname = apartner_nachname;
            }
        }

        @DatabaseTable(tableName = "AuftragsstatusObjBean")
        public static class AuftragsstatusObjBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private String auftragsstatus;

            @DatabaseField
            private int auftragsstatus_lfdnr;

            @DatabaseField
            private String label;

            @DatabaseField
            private String bemerkung_intern;

            @DatabaseField
            private String hinweis_extern;

            @DatabaseField
            private String ist_aktiv;

            @DatabaseField
            private String ibring_relevant;

            @DatabaseField
            private String ist_pa_aenderbar;

            @DatabaseField
            private String ist_pa_stornierbar;

            @DatabaseField
            private String ist_kd_aenderbar;

            @DatabaseField
            private String ist_kd_stornierbar;

            @DatabaseField
            private String ist_erledigt;

            @DatabaseField
            private String ist_abrechenbar;

            public AuftragsstatusObjBean() {
            }

            public AuftragsstatusObjBean(Integer ID,
                                         String auftragsstatus,
                                         int auftragsstatus_lfdnr,
                                         String label,
                                         String bemerkung_intern,
                                         String hinweis_extern,
                                         String ist_aktiv,
                                         String ibring_relevant,
                                         String ist_pa_aenderbar,
                                         String ist_pa_stornierbar,
                                         String ist_kd_aenderbar,
                                         String ist_erledigt,
                                         String ist_kd_stornierbar,
                                         String ist_abrechenbar) {
                this.ID = ID;
                this.auftragsstatus = auftragsstatus;
                this.auftragsstatus_lfdnr = auftragsstatus_lfdnr;
                this.label = label;
                this.bemerkung_intern = bemerkung_intern;
                this.hinweis_extern = hinweis_extern;
                this.ist_aktiv = ist_aktiv;
                this.ibring_relevant = ibring_relevant;
                this.ist_pa_aenderbar = ist_pa_aenderbar;
                this.ist_pa_stornierbar = ist_pa_stornierbar;
                this.ist_kd_aenderbar = ist_kd_aenderbar;
                this.ist_erledigt = ist_erledigt;
                this.ist_kd_stornierbar = ist_kd_stornierbar;
                this.ist_abrechenbar = ist_abrechenbar;
            }

            public String getAuftragsstatus() {
                return auftragsstatus;
            }

            public void setAuftragsstatus(String auftragsstatus) {
                this.auftragsstatus = auftragsstatus;
            }

            public int getAuftragsstatus_lfdnr() {
                return auftragsstatus_lfdnr;
            }

            public void setAuftragsstatus_lfdnr(int auftragsstatus_lfdnr) {
                this.auftragsstatus_lfdnr = auftragsstatus_lfdnr;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }

            public String getBemerkung_intern() {
                return bemerkung_intern;
            }

            public void setBemerkung_intern(String bemerkung_intern) {
                this.bemerkung_intern = bemerkung_intern;
            }

            public String getHinweis_extern() {
                return hinweis_extern;
            }

            public void setHinweis_extern(String hinweis_extern) {
                this.hinweis_extern = hinweis_extern;
            }

            public String getIst_aktiv() {
                return ist_aktiv;
            }

            public void setIst_aktiv(String ist_aktiv) {
                this.ist_aktiv = ist_aktiv;
            }

            public String getIbring_relevant() {
                return ibring_relevant;
            }

            public void setIbring_relevant(String ibring_relevant) {
                this.ibring_relevant = ibring_relevant;
            }

            public String getIst_pa_aenderbar() {
                return ist_pa_aenderbar;
            }

            public void setIst_pa_aenderbar(String ist_pa_aenderbar) {
                this.ist_pa_aenderbar = ist_pa_aenderbar;
            }

            public String getIst_pa_stornierbar() {
                return ist_pa_stornierbar;
            }

            public void setIst_pa_stornierbar(String ist_pa_stornierbar) {
                this.ist_pa_stornierbar = ist_pa_stornierbar;
            }

            public String getIst_kd_aenderbar() {
                return ist_kd_aenderbar;
            }

            public void setIst_kd_aenderbar(String ist_kd_aenderbar) {
                this.ist_kd_aenderbar = ist_kd_aenderbar;
            }

            public String getIst_kd_stornierbar() {
                return ist_kd_stornierbar;
            }

            public void setIst_kd_stornierbar(String ist_kd_stornierbar) {
                this.ist_kd_stornierbar = ist_kd_stornierbar;
            }

            public String getIst_erledigt() {
                return ist_erledigt;
            }

            public void setIst_erledigt(String ist_erledigt) {
                this.ist_erledigt = ist_erledigt;
            }

            public String getIst_abrechenbar() {
                return ist_abrechenbar;
            }

            public void setIst_abrechenbar(String ist_abrechenbar) {
                this.ist_abrechenbar = ist_abrechenbar;
            }
        }

        @DatabaseTable(tableName = "OrderItemsBean")
        public static class OrderItemsBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private int anzahl;

            @DatabaseField
            private int stg_katalog_fk;

            @DatabaseField
            private String stg_katalog_label;

            @DatabaseField
            private String label;

            @DatabaseField
            private int laenge_cm;

            @DatabaseField
            private int breite_cm;

            @DatabaseField
            private int hoehe_cm;

            @DatabaseField
            private int gewicht_kg;

            @DatabaseField
            private String ibring_typ;

            public OrderItemsBean() {
            }

            public OrderItemsBean(String ibring_typ,
                                  int gewicht_kg,
                                  int hoehe_cm,
                                  int breite_cm,
                                  int laenge_cm,
                                  String label,
                                  String stg_katalog_label,
                                  int stg_katalog_fk,
                                  int anzahl,
                                  int pk,
                                  Integer ID) {
                this.ibring_typ = ibring_typ;
                this.gewicht_kg = gewicht_kg;
                this.hoehe_cm = hoehe_cm;
                this.breite_cm = breite_cm;
                this.laenge_cm = laenge_cm;
                this.label = label;
                this.stg_katalog_label = stg_katalog_label;
                this.stg_katalog_fk = stg_katalog_fk;
                this.anzahl = anzahl;
                this.pk = pk;
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public int getAnzahl() {
                return anzahl;
            }

            public void setAnzahl(int anzahl) {
                this.anzahl = anzahl;
            }

            public int getStg_katalog_fk() {
                return stg_katalog_fk;
            }

            public void setStg_katalog_fk(int stg_katalog_fk) {
                this.stg_katalog_fk = stg_katalog_fk;
            }

            public String getStg_katalog_label() {
                return stg_katalog_label;
            }

            public void setStg_katalog_label(String stg_katalog_label) {
                this.stg_katalog_label = stg_katalog_label;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }

            public int getLaenge_cm() {
                return laenge_cm;
            }

            public void setLaenge_cm(int laenge_cm) {
                this.laenge_cm = laenge_cm;
            }

            public int getBreite_cm() {
                return breite_cm;
            }

            public void setBreite_cm(int breite_cm) {
                this.breite_cm = breite_cm;
            }

            public int getHoehe_cm() {
                return hoehe_cm;
            }

            public void setHoehe_cm(int hoehe_cm) {
                this.hoehe_cm = hoehe_cm;
            }

            public int getGewicht_kg() {
                return gewicht_kg;
            }

            public void setGewicht_kg(int gewicht_kg) {
                this.gewicht_kg = gewicht_kg;
            }

            public String getIbring_typ() {
                return ibring_typ;
            }

            public void setIbring_typ(String ibring_typ) {
                this.ibring_typ = ibring_typ;
            }
        }

        @DatabaseTable(tableName = "OrderStatusLogBean")
        public static class OrderStatusLogBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private int pk;

            @DatabaseField
            private int lfdnr;

            @DatabaseField
            private int auftrstatus_fk;

            @DatabaseField
            private String auftragsstatus;

            @DatabaseField
            private String label;

            @DatabaseField
            private String bemerkung_intern;

            @DatabaseField
            private String hinweis_extern;

            @DatabaseField
            private String ist_aktiv;

            @DatabaseField
            private String ibring_relevant;

            @DatabaseField
            private String ist_pa_aenderbar;

            @DatabaseField
            private String ist_pa_stornierbar;

            @DatabaseField
            private String ist_kd_aenderbar;

            @DatabaseField
            private String ist_kd_stornierbar;

            @DatabaseField
            private String ist_erledigt;

            @DatabaseField
            private String ist_abrechenbar;

            @DatabaseField
            private String anlagedat;

            public OrderStatusLogBean() {
            }

            public OrderStatusLogBean(Integer ID,
                                      int pk,
                                      int lfdnr,
                                      int auftrstatus_fk,
                                      String auftragsstatus,
                                      String label,
                                      String bemerkung_intern,
                                      String hinweis_extern,
                                      String ist_aktiv,
                                      String ibring_relevant,
                                      String ist_pa_aenderbar,
                                      String ist_pa_stornierbar,
                                      String ist_kd_aenderbar,
                                      String ist_kd_stornierbar,
                                      String ist_erledigt,
                                      String ist_abrechenbar,
                                      String anlagedat) {
                this.ID = ID;
                this.pk = pk;
                this.lfdnr = lfdnr;
                this.auftrstatus_fk = auftrstatus_fk;
                this.auftragsstatus = auftragsstatus;
                this.label = label;
                this.bemerkung_intern = bemerkung_intern;
                this.hinweis_extern = hinweis_extern;
                this.ist_aktiv = ist_aktiv;
                this.ibring_relevant = ibring_relevant;
                this.ist_pa_aenderbar = ist_pa_aenderbar;
                this.ist_pa_stornierbar = ist_pa_stornierbar;
                this.ist_kd_aenderbar = ist_kd_aenderbar;
                this.ist_kd_stornierbar = ist_kd_stornierbar;
                this.ist_erledigt = ist_erledigt;
                this.ist_abrechenbar = ist_abrechenbar;
                this.anlagedat = anlagedat;
            }

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public int getLfdnr() {
                return lfdnr;
            }

            public void setLfdnr(int lfdnr) {
                this.lfdnr = lfdnr;
            }

            public int getAuftrstatus_fk() {
                return auftrstatus_fk;
            }

            public void setAuftrstatus_fk(int auftrstatus_fk) {
                this.auftrstatus_fk = auftrstatus_fk;
            }

            public String getAuftragsstatus() {
                return auftragsstatus;
            }

            public void setAuftragsstatus(String auftragsstatus) {
                this.auftragsstatus = auftragsstatus;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }

            public String getBemerkung_intern() {
                return bemerkung_intern;
            }

            public void setBemerkung_intern(String bemerkung_intern) {
                this.bemerkung_intern = bemerkung_intern;
            }

            public String getHinweis_extern() {
                return hinweis_extern;
            }

            public void setHinweis_extern(String hinweis_extern) {
                this.hinweis_extern = hinweis_extern;
            }

            public String getIst_aktiv() {
                return ist_aktiv;
            }

            public void setIst_aktiv(String ist_aktiv) {
                this.ist_aktiv = ist_aktiv;
            }

            public String getIbring_relevant() {
                return ibring_relevant;
            }

            public void setIbring_relevant(String ibring_relevant) {
                this.ibring_relevant = ibring_relevant;
            }

            public String getIst_pa_aenderbar() {
                return ist_pa_aenderbar;
            }

            public void setIst_pa_aenderbar(String ist_pa_aenderbar) {
                this.ist_pa_aenderbar = ist_pa_aenderbar;
            }

            public String getIst_pa_stornierbar() {
                return ist_pa_stornierbar;
            }

            public void setIst_pa_stornierbar(String ist_pa_stornierbar) {
                this.ist_pa_stornierbar = ist_pa_stornierbar;
            }

            public String getIst_kd_aenderbar() {
                return ist_kd_aenderbar;
            }

            public void setIst_kd_aenderbar(String ist_kd_aenderbar) {
                this.ist_kd_aenderbar = ist_kd_aenderbar;
            }

            public String getIst_kd_stornierbar() {
                return ist_kd_stornierbar;
            }

            public void setIst_kd_stornierbar(String ist_kd_stornierbar) {
                this.ist_kd_stornierbar = ist_kd_stornierbar;
            }

            public String getIst_erledigt() {
                return ist_erledigt;
            }

            public void setIst_erledigt(String ist_erledigt) {
                this.ist_erledigt = ist_erledigt;
            }

            public String getIst_abrechenbar() {
                return ist_abrechenbar;
            }

            public void setIst_abrechenbar(String ist_abrechenbar) {
                this.ist_abrechenbar = ist_abrechenbar;
            }

            public String getAnlagedat() {
                return anlagedat;
            }

            public void setAnlagedat(String anlagedat) {
                this.anlagedat = anlagedat;
            }
        }
    }
}
