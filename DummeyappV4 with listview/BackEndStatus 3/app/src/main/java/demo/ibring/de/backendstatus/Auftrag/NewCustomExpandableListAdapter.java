package demo.ibring.de.backendstatus.Auftrag;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.commons.collections4.map.ListOrderedMap;

import java.util.List;

import demo.ibring.de.backendstatus.Auftrag_Activity;
import demo.ibring.de.backendstatus.R;

/**
 * Created by albert on 09/08/16.
 */
public class NewCustomExpandableListAdapter extends BaseExpandableListAdapter {

    EditText zp_PLZ;
    EditText zp_Strasse;
    EditText zp_StrasseNummer;
    EditText zp_Ort;

    Spinner spinner;

    private Context context;
    private List<String> expandableListTitle;
    private ListOrderedMap<String, List<Object>> expandableListDetail;

    public NewCustomExpandableListAdapter(Context context, List<String> expandableListTitle, ListOrderedMap<String, List<Object>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (("Auftragsgeber").equals(expandableListTitle.get(listPosition))) {
                convertView = layoutInflater.inflate(R.layout.listview_ag, null);
                TextView expandedListTextView = (TextView) convertView.findViewById(R.id.ag_textview);
                expandedListTextView.setText(expandedListText);
            }else if (("Zustellpartner").equals(expandableListTitle.get(listPosition))){
                convertView = layoutInflater.inflate(R.layout.listview_zp, null);
                zp_PLZ = (EditText) convertView.findViewById(R.id.zp_plz);
                //zp_Strasse = (EditText) convertView.findViewById(R.id.zp_strasse);
                zp_StrasseNummer = (EditText) convertView.findViewById(R.id.zp_strasse_nr);
                zp_Ort = (EditText) convertView.findViewById(R.id.zp_ort);
                zp_Ort.setText("TESTT");
                spinner = (Spinner) convertView.findViewById(R.id.auftrag_spinner);


                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.planets_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                //addItemsOnSpinner2();
                spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());

            }
        }else{
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (("Auftragsgeber").equals(expandableListTitle.get(listPosition))) {
                convertView = layoutInflater.inflate(R.layout.listview_ag, null);
                TextView expandedListTextView = (TextView) convertView.findViewById(R.id.ag_textview);
                expandedListTextView.setText(expandedListText);
            }else if (("Zustellpartner").equals(expandableListTitle.get(listPosition))){
                convertView = layoutInflater.inflate(R.layout.listview_zp, null);
                zp_PLZ = (EditText) convertView.findViewById(R.id.zp_plz);
                //zp_Strasse = (EditText) convertView.findViewById(R.id.zp_strasse);
                zp_StrasseNummer = (EditText) convertView.findViewById(R.id.zp_strasse_nr);
                zp_Ort = (EditText) convertView.findViewById(R.id.zp_ort);
                zp_Ort.setText("TESTT");
                spinner = (Spinner) convertView.findViewById(R.id.auftrag_spinner);

                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context, R.array.planets_array, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                //addItemsOnSpinner2();
                spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());

            }

        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.listview_titles, null);

        }

        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }


}


