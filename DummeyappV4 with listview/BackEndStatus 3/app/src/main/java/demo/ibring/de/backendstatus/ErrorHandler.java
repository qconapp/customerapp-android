package demo.ibring.de.backendstatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by albert on 04/08/16.
 */
public class ErrorHandler {

    public List<String> errorList(JSONObject responseObject){
        List<String> errorStringList = new ArrayList<String>();
        try {
            JSONObject errorOBJ = responseObject.getJSONObject("error_obj");
            for (int i = 0; i < errorOBJ.length(); i++){

                JSONObject eroorElement = errorOBJ.getJSONObject(errorOBJ.toString());

                errorStringList.add(eroorElement.names().get(i).toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return errorStringList;


    }

    public boolean emailExist (List<String> errorList){
        boolean error = false;
        String emailEroor = "email";
        for (int i = 0; i < errorList.size(); i++){
            if (emailEroor.equalsIgnoreCase(errorList.get(i))){
                error = true;

            }
        }
        return error;
    }
}
