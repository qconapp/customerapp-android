package demo.ibring.de.backendstatus;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by albert on 27/07/16.
 */
public class RegisterVolley {

    private String URL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/v3/register/";

    RequestQueue requestQueue;


    public void registerUser(Context context, JSONObject jsonObject, final registerVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);


    }



    public interface registerVolleyCallback{
        void onSuccess(String response);
        void onFail(VolleyError error);
    }



}
