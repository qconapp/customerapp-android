package demo.ibring.de.backendstatus;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.query.In;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by eliebitar on 27/07/16.
 */
public class CustomersFinal {
    /**
     *
     *
     * ItemsBean Table
     * IteamsBean JSON Class
     *
     * */
    private List<ItemsBean> items;

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    @DatabaseTable(tableName = "ItemsBean")
    public static class ItemsBean {


        @DatabaseField(id = true)
        private Integer pk;

        @DatabaseField
        private String debitor;

        @DatabaseField
        private String klasse;

        @DatabaseField
        private String gast;

        @DatabaseField
        private String name1;

        @DatabaseField
        private String name2;

        @DatabaseField
        private String apartner_anrede_kennz;

        @DatabaseField
        private String apartner_vorname;

        @DatabaseField
        private String apartner_nachname;

        @DatabaseField
        private String geburtsdatum;

        @DatabaseField
        private String zahlziel_kennz;

        @DatabaseField
        private String vertragsbeginn;

        @DatabaseField
        private String ustandort_kennz;


        private Object related_customer;


        private String related_customer_kennz;

        @DatabaseField
        private String aktiv;

        @DatabaseField
        private Date date;

        public ItemsBean(Integer pk,
                         String debitor,
                         String klasse,
                         String gast,
                         String name1,
                         String name2,
                         String apartner_anrede_kennz,
                         String apartner_vorname,
                         String apartner_nachname,
                         String geburtsdatum,
                         String zahlziel_kennz,
                         String vertragsbeginn,
                         String ustandort_kennz,
                         String aktiv) {
            this.pk = pk;
            this.debitor = debitor;
            this.klasse = klasse;
            this.gast = gast;
            this.name1 = name1;
            this.name2 = name2;
            this.apartner_anrede_kennz = apartner_anrede_kennz;
            this.apartner_vorname = apartner_vorname;
            this.apartner_nachname = apartner_nachname;
            this.geburtsdatum = geburtsdatum;
            this.zahlziel_kennz = zahlziel_kennz;
            this.vertragsbeginn = vertragsbeginn;
            this.ustandort_kennz = ustandort_kennz;
            this.aktiv = aktiv;
            this.date = new Date(System.currentTimeMillis());
        }

        public ItemsBean() {
        }

        private List<AddressesBean> addresses;
        private List<EmailAddressesBean> email_addresses;
        private List<PhoneNumbersBean> phone_numbers;
        private List<?> open_hours;
        private List<?> description;

        public int getPk() {
            return pk;
        }

        public void setPk(int pk) {
            this.pk = pk;
        }

        public String getDebitor() {
            return debitor;
        }

        public void setDebitor(String debitor) {
            this.debitor = debitor;
        }

        public String getKlasse() {
            return klasse;
        }

        public void setKlasse(String klasse) {
            this.klasse = klasse;
        }

        public String getGast() {
            return gast;
        }

        public void setGast(String gast) {
            this.gast = gast;
        }

        public String getName1() {
            return name1;
        }

        public void setName1(String name1) {
            this.name1 = name1;
        }

        public String getName2() {
            return name2;
        }

        public void setName2(String name2) {
            this.name2 = name2;
        }

        public String getApartner_anrede_kennz() {
            return apartner_anrede_kennz;
        }

        public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
            this.apartner_anrede_kennz = apartner_anrede_kennz;
        }

        public String getApartner_vorname() {
            return apartner_vorname;
        }

        public void setApartner_vorname(String apartner_vorname) {
            this.apartner_vorname = apartner_vorname;
        }

        public String getApartner_nachname() {
            return apartner_nachname;
        }

        public void setApartner_nachname(String apartner_nachname) {
            this.apartner_nachname = apartner_nachname;
        }

        public String getGeburtsdatum() {
            return geburtsdatum;
        }

        public void setGeburtsdatum(String geburtsdatum) {
            this.geburtsdatum = geburtsdatum;
        }

        public String getZahlziel_kennz() {
            return zahlziel_kennz;
        }

        public void setZahlziel_kennz(String zahlziel_kennz) {
            this.zahlziel_kennz = zahlziel_kennz;
        }

        public String getVertragsbeginn() {
            return vertragsbeginn;
        }

        public void setVertragsbeginn(String vertragsbeginn) {
            this.vertragsbeginn = vertragsbeginn;
        }

        public String getUstandort_kennz() {
            return ustandort_kennz;
        }

        public void setUstandort_kennz(String ustandort_kennz) {
            this.ustandort_kennz = ustandort_kennz;
        }

        public Object getRelated_customer() {
            return related_customer;
        }

        public void setRelated_customer(Object related_customer) {
            this.related_customer = related_customer;
        }

        public String getRelated_customer_kennz() {
            return related_customer_kennz;
        }

        public void setRelated_customer_kennz(String related_customer_kennz) {
            this.related_customer_kennz = related_customer_kennz;
        }

        public String getAktiv() {
            return aktiv;
        }

        public void setAktiv(String aktiv) {
            this.aktiv = aktiv;
        }

        public List<AddressesBean> getAddresses() {
            return addresses;
        }

        public void setAddresses(List<AddressesBean> addresses) {
            this.addresses = addresses;
        }

        public List<EmailAddressesBean> getEmail_addresses() {
            return email_addresses;
        }

        public void setEmail_addresses(List<EmailAddressesBean> email_addresses) {
            this.email_addresses = email_addresses;
        }

        public List<PhoneNumbersBean> getPhone_numbers() {
            return phone_numbers;
        }

        public void setPhone_numbers(List<PhoneNumbersBean> phone_numbers) {
            this.phone_numbers = phone_numbers;
        }

        public List<?> getOpen_hours() {
            return open_hours;
        }

        public void setOpen_hours(List<?> open_hours) {
            this.open_hours = open_hours;
        }

        public List<?> getDescription() {
            return description;
        }

        public void setDescription(List<?> description) {
            this.description = description;
        }

        /**
         *
         * AddressesBean Table
         * AddressesBean JSON class
         *
         */

        @DatabaseTable(tableName = "AddressesBean")
        public static class AddressesBean {


            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private Integer pk;

            @DatabaseField
            private Integer prio;

            @DatabaseField
            private String rechadr;

            @DatabaseField
            private String liefadr;

            @DatabaseField
            private String zustadr;

            @DatabaseField
            private String strasse;

            @DatabaseField
            private String hausnr;

            @DatabaseField
            private String adresszusatz;

            @DatabaseField
            private String plz;

            @DatabaseField
            private String ort;

            @DatabaseField
            private String land_kennz;

            @DatabaseField
            private String name1;

            @DatabaseField
            private String name2;

            @DatabaseField
            private String apartner_anrede_kennz;

            @DatabaseField
            private String apartner_vorname;

            @DatabaseField
            private String apartner_nachname;

            @DatabaseField
            private double latitude;

            @DatabaseField
            private double longitude;

            @DatabaseField
            private Date date;

            public AddressesBean(Integer ID,
                                 Integer pk,
                                 Integer prio,
                                 String  rechadr,
                                 String  liefadr,
                                 String  strasse,
                                 String  zustadr,
                                 String  hausnr,
                                 String  adresszusatz,
                                 String  plz,
                                 String  ort,
                                 String  land_kennz,
                                 String  name1,
                                 String  name2,
                                 String  apartner_anrede_kennz,
                                 String  apartner_vorname,
                                 String  apartner_nachname,
                                 double  latitude,
                                 double  longitude) {

                //this.ref                   = ref;
                this.ID                    = ID;
                this.pk                    = pk;
                this.prio                  = prio;
                this.rechadr               = rechadr;
                this.liefadr               = liefadr;
                this.strasse               = strasse;
                this.zustadr               = zustadr;
                this.hausnr                = hausnr;
                this.adresszusatz          = adresszusatz;
                this.plz                   = plz;
                this.ort                   = ort;
                this.land_kennz            = land_kennz;
                this.name1                 = name1;
                this.name2                 = name2;
                this.apartner_anrede_kennz = apartner_anrede_kennz;
                this.apartner_vorname      = apartner_vorname;
                this.apartner_nachname     = apartner_nachname;
                this.latitude              = latitude;
                this.longitude             = longitude;
                this.date                  = new Date(System.currentTimeMillis());
            }

            public AddressesBean() {
            }

            public Integer getID() {
                return ID;
            }

            public void setID(Integer ID) {
                this.ID = ID;
            }
            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public int getPrio() {
                return prio;
            }

            public void setPrio(int prio) {
                this.prio = prio;
            }

            public String getRechadr() {
                return rechadr;
            }

            public void setRechadr(String rechadr) {
                this.rechadr = rechadr;
            }

            public String getLiefadr() {
                return liefadr;
            }

            public void setLiefadr(String liefadr) {
                this.liefadr = liefadr;
            }

            public String getZustadr() {
                return zustadr;
            }

            public void setZustadr(String zustadr) {
                this.zustadr = zustadr;
            }

            public String getStrasse() {
                return strasse;
            }

            public void setStrasse(String strasse) {
                this.strasse = strasse;
            }

            public String getHausnr() {
                return hausnr;
            }

            public void setHausnr(String hausnr) {
                this.hausnr = hausnr;
            }

            public String getAdresszusatz() {
                return adresszusatz;
            }

            public void setAdresszusatz(String adresszusatz) {
                this.adresszusatz = adresszusatz;
            }

            public String getPlz() {
                return plz;
            }

            public void setPlz(String plz) {
                this.plz = plz;
            }

            public String getOrt() {
                return ort;
            }

            public void setOrt(String ort) {
                this.ort = ort;
            }

            public String getLand_kennz() {
                return land_kennz;
            }

            public void setLand_kennz(String land_kennz) {
                this.land_kennz = land_kennz;
            }

            public String getName1() {
                return name1;
            }

            public void setName1(String name1) {
                this.name1 = name1;
            }

            public String getName2() {
                return name2;
            }

            public void setName2(String name2) {
                this.name2 = name2;
            }

            public String getApartner_anrede_kennz() {
                return apartner_anrede_kennz;
            }

            public void setApartner_anrede_kennz(String apartner_anrede_kennz) {
                this.apartner_anrede_kennz = apartner_anrede_kennz;
            }

            public String getApartner_vorname() {
                return apartner_vorname;
            }

            public void setApartner_vorname(String apartner_vorname) {
                this.apartner_vorname = apartner_vorname;
            }

            public String getApartner_nachname() {
                return apartner_nachname;
            }

            public void setApartner_nachname(String apartner_nachname) {
                this.apartner_nachname = apartner_nachname;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }
        }

        /**
         *
         * EmailAddressesBean Table
         * EmailAddressesBean JSON class
         *
         */

        @DatabaseTable(tableName = "EmailAddressesBean")
        public static class EmailAddressesBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private Integer pk;

            @DatabaseField
            private String standard;

            @DatabaseField
            private String email;

            @DatabaseField
            private Date date;

            public EmailAddressesBean(Integer pk, String standard, String email) {
                this.pk = pk;
                this.standard = standard;
                this.email = email;
                this.date = new Date(System.currentTimeMillis());
            }

            public EmailAddressesBean() {
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getStandard() {
                return standard;
            }

            public void setStandard(String standard) {
                this.standard = standard;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }
        }

        /**
         *
         * PhoneNumbersBean Table
         * PhoneNumbersBean JSON class
         *
         */

        @DatabaseTable(tableName = "PhoneNumbersBean")
        public static class PhoneNumbersBean {

            @DatabaseField(generatedId = true, unique = true)
            private Integer ID;

            @DatabaseField
            private Integer pk;

            @DatabaseField
            private String typ;

            @DatabaseField
            private String standard;

            @DatabaseField
            private String rufnummer;

            @DatabaseField
            private String bemerkung;

            @DatabaseField
            private Date date;

            public PhoneNumbersBean(Integer pk, String typ, String standard, String rufnummer, String bemerkung) {
                this.pk = pk;
                this.typ = typ;
                this.standard = standard;
                this.rufnummer = rufnummer;
                this.bemerkung = bemerkung;
                this.date = new Date(System.currentTimeMillis());
            }

            public PhoneNumbersBean() {
            }

            public int getPk() {
                return pk;
            }

            public void setPk(int pk) {
                this.pk = pk;
            }

            public String getTyp() {
                return typ;
            }

            public void setTyp(String typ) {
                this.typ = typ;
            }

            public String getStandard() {
                return standard;
            }

            public void setStandard(String standard) {
                this.standard = standard;
            }

            public String getRufnummer() {
                return rufnummer;
            }

            public void setRufnummer(String rufnummer) {
                this.rufnummer = rufnummer;
            }

            public String getBemerkung() {
                return bemerkung;
            }

            public void setBemerkung(String bemerkung) {
                this.bemerkung = bemerkung;
            }
        }
    }
}
