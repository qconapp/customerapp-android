package demo.ibring.de.backendstatus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.view.View;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by albert on 26/07/16.
 */
public class Validator {

    private final static String DATE_FORMAT = "dd.MM.yyyy";

    public boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[A-Z])(?=.*[a-z]).{8,25}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
    public int isValidPassword2(final String password) {

        Pattern patternlength;
        Matcher matcherlength;
        Pattern patternbig;
        Matcher matcherbig;
        Pattern patternsmall;
        Matcher matchersmall;


        final String PASSWORD_PATTERN_LENGTH = "^.{8,25}$";
        final String PASSWORD_PATTERN_BIG = "^(?=.*[A-Z]).{8,25}$";
        final String PASSWORD_PATTERN_SMALL = "^(?=.*[a-z]).{8,25}$";


        patternlength = Pattern.compile(PASSWORD_PATTERN_LENGTH);
        matcherlength = patternlength.matcher(password);

        patternbig = Pattern.compile(PASSWORD_PATTERN_BIG);
        matcherbig = patternbig.matcher(password);

        patternsmall = Pattern.compile(PASSWORD_PATTERN_SMALL);
        matchersmall = patternsmall.matcher(password);

        if (matcherlength.matches()){
            if (matcherbig.matches()){
                if (matchersmall.matches()){
                    return 1;
                }else{
                    return -1;
                }
            }else{
                return -2;
            }
        }else {
            return -3;
        }

    }



    /*public  boolean isDateValid(String date) throws java.text.ParseException {
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }*/

    public  boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String getIP(Context context){
        WifiManager wifiMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipAddress = Formatter.formatIpAddress(ip);
        return ipAddress;
    }

    public String getVersion (Context context){
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        return version;
    }

    public void showErrorMessage(String message, Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setMessage(message);
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    public boolean validateEditText(EditText[] fields){
        for(int i=0; i<fields.length; i++){
            EditText currentField=fields[i];
            if(currentField.getText().toString().isEmpty()){
                //currentField.setError("Please Fill the field");
                currentField.setFocusable(true);
                return false;
            }
        }
        return true;
    }

    public void focusLisnet(final EditText editText, final String checkMs, final String errorMessage, final Drawable xicon, final Drawable checkIcon){
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){

                    if (!editText.getText().toString().isEmpty()) {
                        editText.setError(checkMs, checkIcon);
                    }else{

                        editText.setError(errorMessage, xicon);


                    }
                }else if (hasFocus){
                    if (editText.getText().toString().isEmpty()) {

                        editText.setError(errorMessage);

                    }
                }
            }
        });
    }
    public void emailfocusLisnet(final EditText editText, final String checkMs, final String errorMessage, final Drawable xicon, final Drawable checkIcon){
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){



                    if (!editText.getText().toString().isEmpty()) {
                        if (isEmailValid(editText.getText().toString())) {
                            editText.setError(checkMs, checkIcon);
                        }else{
                            editText.setError("Invalid Email Address", xicon);
                        }
                    }else{

                        editText.setError(errorMessage, xicon);
                    }
                }else if (hasFocus){
                    if (editText.getText().toString().isEmpty()) {

                        editText.setError(errorMessage);
                    }
                }
            }
        });
    }
    public void passwordfocusLisnet(final EditText editText, final String checkMs, final String errorMessage, final Drawable xicon, final Drawable checkIcon){
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){



                    if (!editText.getText().toString().isEmpty()) {
                        if (isValidPassword(editText.getText().toString())) {
                            editText.setError(checkMs, checkIcon);
                        }else{
                            editText.setError("Invalid password. password must be at least 8 characters and contain big and small letters ", xicon);
                        }
                    }else{
                        editText.setError(errorMessage, xicon);
                    }
                }else if (hasFocus){
                    if (editText.getText().toString().isEmpty()) {

                        editText.setError(errorMessage);
                    }
                }
            }
        });
    }
    public void passwordRepeatfocusLisnet(final EditText password, final EditText passwordrepeat, final String checkMs, final String errorMessage, final Drawable xicon, final Drawable checkIcon){
        passwordrepeat.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){



                    if (!passwordrepeat.getText().toString().isEmpty()) {
                        if (password.getText().toString().equals(passwordrepeat.getText().toString())) {
                            passwordrepeat.setError(checkMs, checkIcon);
                        }else{
                            passwordrepeat.setError("password does not match ", xicon);
                        }
                    }else{
                        passwordrepeat.setError(errorMessage, xicon);
                    }
                }else if (hasFocus){
                    if (passwordrepeat.getText().toString().isEmpty()) {

                        passwordrepeat.setError(errorMessage);
                    }
                }
            }
        });
    }

    public void datefocusLisnet(final EditText editText, final String checkMs, final String errorMessage, final Drawable xicon, final Drawable checkIcon){
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){



                    if (!editText.getText().toString().isEmpty()) {
                        if (isValidDate(editText.getText().toString())) {
                            editText.setError(checkMs, checkIcon);
                        }else{
                            editText.setError("invalid date or date format. format must bet tt.MM.YYYY", xicon);
                        }
                    }else{
                        editText.setError(errorMessage, xicon);
                    }
                }else if (hasFocus){
                    if (editText.getText().toString().isEmpty()) {

                        editText.setError(errorMessage);
                    }
                }
            }
        });
    }








}
