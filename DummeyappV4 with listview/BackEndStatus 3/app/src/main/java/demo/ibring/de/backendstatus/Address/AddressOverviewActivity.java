package demo.ibring.de.backendstatus.Address;


import android.content.Intent;
import android.media.Image;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import demo.ibring.de.backendstatus.CustomersFinal;
import demo.ibring.de.backendstatus.DB.DataBaseOperation;
import demo.ibring.de.backendstatus.DB.DatabaseHelper;
import demo.ibring.de.backendstatus.Log_In;
import demo.ibring.de.backendstatus.MyListAdapter;
import demo.ibring.de.backendstatus.R;

public class AddressOverviewActivity extends AppCompatActivity {

    List<CustomersFinal.ItemsBean.AddressesBean>         mAddressList;
    DatabaseHelper                                       db;
    Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> addressesBeenDao;
    DataBaseOperation                                    dataBaseOperation;
    ListView                                             listView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_overview);

        ImageView mImageView = (ImageView) findViewById(R.id.imageView);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddressOverviewActivity.this, Log_In.class);
                startActivity(intent);
            }
        });

        //getApplicationContext().deleteDatabase("customres");

        db                = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        dataBaseOperation = new DataBaseOperation(db);
        listView          = (ListView) findViewById(R.id.listView);

        try {
            dataBaseOperation.InsertOrUpdateTheTable(new CustomersFinal.ItemsBean.AddressesBean(
                    1,
                    421078,
                    23,
                    "sdf",
                    "asdf",
                    "saedf",
                    "asd",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    "sdgfsdf",
                    112,
                    3334));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            addressesBeenDao = db.getAddressesBeenDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //************************************************************************************************
        try {
            mAddressList = addressesBeenDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //************************************************************************************************

        //Add Custom views to the ListView
        populateListView();
        //************************************************************************************************

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddressOverviewActivity.this, AddNewAddressActivity.class);
                intent.putExtra("pk", mAddressList.get(0).getPk());
                startActivity(intent);
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
        //************************************************************************************************

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                view.setSelected(true);

                Object ob = parent.getItemAtPosition(position);
                CustomersFinal.ItemsBean.AddressesBean ad = (CustomersFinal.ItemsBean.AddressesBean) ob;

                Intent intent = new Intent(AddressOverviewActivity.this, AddNewAddressActivity.class);

                intent.putExtra("street", ad.getStrasse());
                intent.putExtra("city", ad.getOrt());
                intent.putExtra("house_number", ad.getHausnr());
                intent.putExtra("plz", ad.getPlz());
                intent.putExtra("country", ad.getLand_kennz());
                intent.putExtra("additional_address", ad.getAdresszusatz());
                intent.putExtra("pk", ad.getPk());
                intent.putExtra("id", ad.getID());

                startActivity(intent);
            }
        });
    }

    private void populateListView() {

        ArrayAdapter<CustomersFinal.ItemsBean.AddressesBean> adapter =
                new MyListAdapter(
                        AddressOverviewActivity.this,
                        R.layout.addressview,
                        mAddressList);

        listView.setAdapter(adapter);
    }
    @Override
    public void onBackPressed() { }
}
