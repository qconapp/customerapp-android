package demo.ibring.de.backendstatus.Address;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import demo.ibring.de.backendstatus.CustomersFinal;
import demo.ibring.de.backendstatus.GetJSONstr;
import demo.ibring.de.backendstatus.R;

public class AddAddressActivity extends AppCompatActivity {

    GetJSONstr getJSONstr;
    CustomersFinal customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Address information is saved", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });

        getJSONstr = new GetJSONstr();
        customer = new CustomersFinal();

        Bundle bundle = getIntent().getExtras();
        String pk = bundle.getString("pk");

        String jsonStr = getJSONstr.readFromIbring(pk);
        Gson gson = new GsonBuilder().create();

        try {
            customer = gson.fromJson(jsonStr, CustomersFinal.class);
        }catch (Exception e){
            Log.d("error loading CustomersFinal","");
        }

        EditText street = (EditText) findViewById(R.id.Strasse_EditText);
        street.setText(customer.getItems().get(0).getAddresses().get(0).getStrasse());

        EditText PLZ = (EditText) findViewById(R.id.PLZ_EditText);
        PLZ.setText(customer.getItems().get(0).getAddresses().get(0).getPlz());

        EditText city = (EditText) findViewById(R.id.City_EditText);
        city.setText(customer.getItems().get(0).getAddresses().get(0).getOrt());

        //String toJSON = gson.toJson(customer.getItems().get(0).getAddresses());

        //String post ;





    }

}
