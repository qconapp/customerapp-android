package demo.ibring.de.backendstatus;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.sql.SQLException;

import demo.ibring.de.backendstatus.DB.DataBaseOperation;
import demo.ibring.de.backendstatus.DB.DatabaseHelper;


public class MainActivity extends AppCompatActivity {

    private String pkValue;
    private String input;
    private CustomersFinal i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button scan = (Button) findViewById(R.id.Scan_Button);
        final EditText in = (EditText) findViewById(R.id.Input_EditText);




        scan.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                String pkInput = in.getText().toString();
                if (!pkInput.isEmpty()){
                    pkValue = pkInput;

                    input = readFromIbring(pkValue);

                    if (input.length() > 18) {
                        Gson gson = new GsonBuilder().create();
                        i = gson.fromJson(input, CustomersFinal.class);

                        try {
                            DataBaseOpo(i);
                        } catch (SQLException e){
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(MainActivity.this, CustomerDataActivity.class);
                        //intent.putExtra("Customer", i);
                        intent.putExtra("Customer_Name", i.getItems().get(0).getName1());
                        //intent.putExtra("Customer_Address_Street", i.getItems().get(0).getAddresses().get(0).getStrasse());
                        intent.putExtra("Customer_pk", pkValue);
                        intent.putExtra("Customer_DateOfBirth", i.getItems().get(0).getGeburtsdatum());
                        startActivity(intent);
                    }
                }
            }
        }) ;



        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    //

    public String readFromIbring(String pk) {
        String json = null;
        String getURL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/customers/"+pk+"/";
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(getURL);
        //httpGet.addHeader("nachname", "isaac");

        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                json = EntityUtils.toString(response.getEntity());
            } else {
            }
        }
        catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     *
     * Insert new Customer into the DB..
     *
     * */

    private void DataBaseOpo(CustomersFinal customer)throws SQLException{

        /*
        *
        * Here to delete the old database in order to generate a new version..
        * */

        getApplicationContext().deleteDatabase("customres");

        DatabaseHelper databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        DataBaseOperation dataBaseOperation = new DataBaseOperation(databaseHelper);

        dataBaseOperation.InsertOrUpdateTheTable(customer.getItems().get(0));
        dataBaseOperation.InsertOrUpdateTheTable(customer.getItems().get(0).getEmail_addresses().get(0));
        dataBaseOperation.InsertOrUpdateTheTable(customer.getItems().get(0).getAddresses().get(0));
        dataBaseOperation.InsertOrUpdateTheTable(customer.getItems().get(0).getPhone_numbers().get(0));



    }

}
