package demo.ibring.de.backendstatus.Address;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import demo.ibring.de.backendstatus.Connection.VolleyAddresses;
import demo.ibring.de.backendstatus.CustomersFinal;
import demo.ibring.de.backendstatus.DB.DataBaseOperation;
import demo.ibring.de.backendstatus.DB.DatabaseHelper;
import demo.ibring.de.backendstatus.R;

public class AddNewAddressActivity extends AppCompatActivity {

    DatabaseHelper                                       mDataBaseHelper;
    DataBaseOperation                                    mDataBaseOperation;
    CustomersFinal.ItemsBean.AddressesBean               mAddressBean;
    Dao<CustomersFinal.ItemsBean.AddressesBean, Integer> mAddressesBeenDao;
    Random                                               mRandom;
    String                                               TAG = "ADD_NEW_ADDRESS_ACTIVITY";
    Integer                                              pk;
    Integer                                              ID;
    VolleyAddresses                                      mVolly;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);

        mDataBaseHelper      = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        mDataBaseOperation   = new DataBaseOperation(mDataBaseHelper);
        mRandom              = new Random();
        mVolly               = new VolleyAddresses();
        ImageView mImageView = (ImageView) findViewById(R.id.imageView1);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddNewAddressActivity.this, AddressOverviewActivity.class);
                startActivity(intent);
            }
        });
        try {
            mAddressesBeenDao  = mDataBaseHelper.getAddressesBeenDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        final EditText mStreet            = (EditText) findViewById(R.id.NEW_ADDRESS_STREET);
        final EditText mCity              = (EditText) findViewById(R.id.NEW_ADDRESS_CITY);
        final EditText mPLZ               = (EditText) findViewById(R.id.NEW_ADDRESS_PLZ);
        final EditText mCountry           = (EditText) findViewById(R.id.NEW_ADDRESS_COUNTRY);
        final EditText mAdditionalAddress = (EditText) findViewById(R.id.NEW_ADDRESS_ADDITIONAL_ADDRESS);
        final EditText mHouseNumber       = (EditText) findViewById(R.id.NEW_ADDRESS_HAOUSNUMBER);


        Intent intent = getIntent();
        try {
            pk = intent.getIntExtra("pk", 0);
        }catch (Exception e){
            Log.d(TAG, "*****ERROR**** cannot convert to Integer..");
        }

        if (intent.hasExtra("id")){
            ID = intent.getIntExtra("id", 0);
        }

        if (intent.hasExtra("street")){

            mStreet.setText(intent.getStringExtra("street"));
            mCity.setText(intent.getStringExtra("city"));
            mPLZ.setText(intent.getStringExtra("plz"));
            mCountry.setText(intent.getStringExtra("country"));
            mAdditionalAddress.setText(intent.getStringExtra("additional_address"));
            mHouseNumber.setText(intent.getStringExtra("house_number"));
        }

        Button mCancel = (Button) findViewById(R.id.NEW_ADDRESS_CANCEL_BUTTON);
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddNewAddressActivity.this, AddressOverviewActivity.class);
                startActivity(intent);
            }
        });

        Button mSave = (Button) findViewById(R.id.NEW_ADDRESS_SAVE_BUTTON);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isEmpty(mStreet) && !isEmpty(mCity) && !isEmpty(mAdditionalAddress) && !isEmpty(mPLZ) && !isEmpty(mCountry)){
                    try {
                        if (ID != null){
                            mAddressBean = CreatAddress(
                                    mStreet,
                                    mPLZ,
                                    mCity,
                                    mAdditionalAddress,
                                    mCountry,
                                    mHouseNumber,
                                    pk,
                                    ID);
                        }else {
                            mAddressBean = CreatAddress(
                                    mStreet,
                                    mPLZ,
                                    mCity,
                                    mAdditionalAddress,
                                    mCountry,
                                    mHouseNumber,
                                    pk);
                        }
                    }catch (Exception e){
                        Log.d(TAG, "*****ERROR****** Creating the new ADDRESSBEAN from the Inputted data...");
                    }

                    try {
                        mDataBaseOperation.InsertOrUpdateTheTable(mAddressBean);
                        Intent intent = new Intent(AddNewAddressActivity.this, AddressOverviewActivity.class);
                        startActivity(intent);
                    } catch (SQLException e) {
                        Log.d(TAG, "*****ERROR****** Insert into the DATABASE");
                    }
                }
            }
        });
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public CustomersFinal.ItemsBean.AddressesBean CreatAddress (EditText str,
                                                                EditText plz,
                                                                EditText city,
                                                                EditText additionalAddress,
                                                                EditText country,
                                                                EditText houseNumber,
                                                                Integer pk) throws SQLException {

        CustomersFinal.ItemsBean.AddressesBean addressesBean = new CustomersFinal.ItemsBean.AddressesBean();

        QueryBuilder<CustomersFinal.ItemsBean.AddressesBean, Integer> queryBuilder = mAddressesBeenDao.queryBuilder();
        queryBuilder.where().eq("pk", pk);
        List<CustomersFinal.ItemsBean.AddressesBean> addresses = queryBuilder.query();


        addressesBean.setPk(pk);
        addressesBean.setPrio(addresses.get(0).getPrio());
        addressesBean.setRechadr(addresses.get(0).getRechadr());
        addressesBean.setLiefadr(addresses.get(0).getLiefadr());
        addressesBean.setZustadr(addresses.get(0).getZustadr());
        addressesBean.setStrasse(str.getText().toString());
        addressesBean.setHausnr(houseNumber.getText().toString());
        addressesBean.setAdresszusatz(additionalAddress.getText().toString());
        addressesBean.setPlz(plz.getText().toString());
        addressesBean.setOrt(city.getText().toString());
        addressesBean.setLand_kennz(country.getText().toString());
        addressesBean.setName1(addresses.get(0).getName1());
        addressesBean.setName2(addresses.get(0).getName2());
        addressesBean.setApartner_anrede_kennz(addresses.get(0).getApartner_anrede_kennz());
        addressesBean.setApartner_vorname(addresses.get(0).getApartner_vorname());
        addressesBean.setApartner_nachname(addresses.get(0).getApartner_nachname());
        addressesBean.setLatitude(addresses.get(0).getLatitude());
        addressesBean.setLongitude(addresses.get(0).getLongitude());


        return addressesBean;
    }

    public CustomersFinal.ItemsBean.AddressesBean CreatAddress (EditText str,
                                                                EditText plz,
                                                                EditText city,
                                                                EditText additionalAddress,
                                                                EditText country,
                                                                EditText houseNumber,
                                                                Integer pk,
                                                                Integer ID) throws SQLException {

        CustomersFinal.ItemsBean.AddressesBean addressesBean = new CustomersFinal.ItemsBean.AddressesBean();
        CustomersFinal.ItemsBean.AddressesBean addresses = mAddressesBeenDao.queryForId(ID);

        addressesBean.setID(ID);
        addressesBean.setPk(pk);
        addressesBean.setPrio(addresses.getPrio());
        addressesBean.setRechadr(addresses.getRechadr());
        addressesBean.setLiefadr(addresses.getLiefadr());
        addressesBean.setZustadr(addresses.getZustadr());
        addressesBean.setStrasse(str.getText().toString());
        addressesBean.setHausnr(houseNumber.getText().toString());
        addressesBean.setAdresszusatz(additionalAddress.getText().toString());
        addressesBean.setPlz(plz.getText().toString());
        addressesBean.setOrt(city.getText().toString());
        addressesBean.setLand_kennz(country.getText().toString());
        addressesBean.setName1(addresses.getName1());
        addressesBean.setName2(addresses.getName2());
        addressesBean.setApartner_anrede_kennz(addresses.getApartner_anrede_kennz());
        addressesBean.setApartner_vorname(addresses.getApartner_vorname());
        addressesBean.setApartner_nachname(addresses.getApartner_nachname());
        addressesBean.setLatitude(addresses.getLatitude());
        addressesBean.setLongitude(addresses.getLongitude());


        return addressesBean;
    }

    @Override
    public void onBackPressed() { }
}
