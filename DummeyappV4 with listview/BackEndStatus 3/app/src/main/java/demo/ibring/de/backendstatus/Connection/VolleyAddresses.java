package demo.ibring.de.backendstatus.Connection;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by eliebitar on 04/08/16.
 */
public class VolleyAddresses {
    RequestQueue requestQueue;


    public void postAddresses(Context context, String url, JSONObject jsonObject, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);


    }

    public void getAddresses(Context context, String url, final AddressesVolleyCallback callback){

        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void deleteAddresses(Context context,String customerPK, String addressPK, final AddressesVolleyCallback callback){

        String URL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/customers/"+customerPK+"/addresses/"+addressPK+"/";
        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error);

            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void putAddresses(Context context, String customerPK, String addressPK, JSONObject jsonObject, final AddressesVolleyCallback callback){

        String URL = "http://213.211.198.51:8081/apex-dev/service/rest/i-bring/frontend/customers/"+customerPK+"/addresses/"+addressPK+"/";


        requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                callback.onSuccess(response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onFail(error);

            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    public interface AddressesVolleyCallback{
        void onSuccess(String response);
        void onFail(VolleyError error);
    }
}

