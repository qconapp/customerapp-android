package demo.ibring.de.backendstatus.Auftrag;

/**
 * Created by albert on 08/08/16.
 */
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class OrderItem {

    @SerializedName("anzahl")
    @Expose
    private Integer anzahl;
    @SerializedName("stg_katalog_fk")
    @Expose
    private Integer stgKatalogFk;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("laenge_cm")
    @Expose
    private Integer laengeCm;
    @SerializedName("breite_cm")
    @Expose
    private Integer breiteCm;
    @SerializedName("hoehe_cm")
    @Expose
    private Integer hoeheCm;
    @SerializedName("gewicht_kg")
    @Expose
    private Integer gewichtKg;

    /**
     *
     * @return
     * The anzahl
     */
    public Integer getAnzahl() {
        return anzahl;
    }

    /**
     *
     * @param anzahl
     * The anzahl
     */
    public void setAnzahl(Integer anzahl) {
        this.anzahl = anzahl;
    }

    /**
     *
     * @return
     * The stgKatalogFk
     */
    public Integer getStgKatalogFk() {
        return stgKatalogFk;
    }

    /**
     *
     * @param stgKatalogFk
     * The stg_katalog_fk
     */
    public void setStgKatalogFk(Integer stgKatalogFk) {
        this.stgKatalogFk = stgKatalogFk;
    }

    /**
     *
     * @return
     * The label
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     * The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     * The laengeCm
     */
    public Integer getLaengeCm() {
        return laengeCm;
    }

    /**
     *
     * @param laengeCm
     * The laenge_cm
     */
    public void setLaengeCm(Integer laengeCm) {
        this.laengeCm = laengeCm;
    }

    /**
     *
     * @return
     * The breiteCm
     */
    public Integer getBreiteCm() {
        return breiteCm;
    }

    /**
     *
     * @param breiteCm
     * The breite_cm
     */
    public void setBreiteCm(Integer breiteCm) {
        this.breiteCm = breiteCm;
    }

    /**
     *
     * @return
     * The hoeheCm
     */
    public Integer getHoeheCm() {
        return hoeheCm;
    }

    /**
     *
     * @param hoeheCm
     * The hoehe_cm
     */
    public void setHoeheCm(Integer hoeheCm) {
        this.hoeheCm = hoeheCm;
    }

    /**
     *
     * @return
     * The gewichtKg
     */
    public Integer getGewichtKg() {
        return gewichtKg;
    }

    /**
     *
     * @param gewichtKg
     * The gewicht_kg
     */
    public void setGewichtKg(Integer gewichtKg) {
        this.gewichtKg = gewichtKg;
    }

}
