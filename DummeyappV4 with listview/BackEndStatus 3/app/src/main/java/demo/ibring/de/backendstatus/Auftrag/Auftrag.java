package demo.ibring.de.backendstatus.Auftrag;

/**
 * Created by albert on 08/08/16.
 */
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Auftrag {

    @SerializedName("auftraggeber_fk")
    @Expose
    private Integer auftraggeberFk;
    @SerializedName("rechempf_fk")
    @Expose
    private Integer rechempfFk;
    @SerializedName("abholpartner_fk")
    @Expose
    private Integer abholpartnerFk;
    @SerializedName("abholadr_fk")
    @Expose
    private Integer abholadrFk;
    @SerializedName("zustellpartner_fk")
    @Expose
    private Integer zustellpartnerFk;
    @SerializedName("zustelladr_fk")
    @Expose
    private Integer zustelladrFk;
    @SerializedName("ustandort_kennz")
    @Expose
    private String ustandortKennz;
    @SerializedName("apartner_anrede_kennz")
    @Expose
    private String apartnerAnredeKennz;
    @SerializedName("apartner_vorname")
    @Expose
    private String apartnerVorname;
    @SerializedName("apartner_nachname")
    @Expose
    private String apartnerNachname;
    @SerializedName("abholen_von")
    @Expose
    private String abholenVon;
    @SerializedName("abholen_bis")
    @Expose
    private String abholenBis;
    @SerializedName("zustellen_von")
    @Expose
    private String zustellenVon;
    @SerializedName("zustellen_bis")
    @Expose
    private String zustellenBis;
    @SerializedName("zustellen_wunsch")
    @Expose
    private String zustellenWunsch;
    @SerializedName("ibring_spezial")
    @Expose
    private String ibringSpezial;
    @SerializedName("auftragsstatus")
    @Expose
    private String auftragsstatus;
    @SerializedName("order_items")
    @Expose
    private List<OrderItem> orderItems = new ArrayList<OrderItem>();

    /**
     *
     * @return
     * The auftraggeberFk
     */
    public Integer getAuftraggeberFk() {
        return auftraggeberFk;
    }

    /**
     *
     * @param auftraggeberFk
     * The auftraggeber_fk
     */
    public void setAuftraggeberFk(Integer auftraggeberFk) {
        this.auftraggeberFk = auftraggeberFk;
    }

    /**
     *
     * @return
     * The rechempfFk
     */
    public Integer getRechempfFk() {
        return rechempfFk;
    }

    /**
     *
     * @param rechempfFk
     * The rechempf_fk
     */
    public void setRechempfFk(Integer rechempfFk) {
        this.rechempfFk = rechempfFk;
    }

    /**
     *
     * @return
     * The abholpartnerFk
     */
    public Integer getAbholpartnerFk() {
        return abholpartnerFk;
    }

    /**
     *
     * @param abholpartnerFk
     * The abholpartner_fk
     */
    public void setAbholpartnerFk(Integer abholpartnerFk) {
        this.abholpartnerFk = abholpartnerFk;
    }

    /**
     *
     * @return
     * The abholadrFk
     */
    public Integer getAbholadrFk() {
        return abholadrFk;
    }

    /**
     *
     * @param abholadrFk
     * The abholadr_fk
     */
    public void setAbholadrFk(Integer abholadrFk) {
        this.abholadrFk = abholadrFk;
    }

    /**
     *
     * @return
     * The zustellpartnerFk
     */
    public Integer getZustellpartnerFk() {
        return zustellpartnerFk;
    }

    /**
     *
     * @param zustellpartnerFk
     * The zustellpartner_fk
     */
    public void setZustellpartnerFk(Integer zustellpartnerFk) {
        this.zustellpartnerFk = zustellpartnerFk;
    }

    /**
     *
     * @return
     * The zustelladrFk
     */
    public Integer getZustelladrFk() {
        return zustelladrFk;
    }

    /**
     *
     * @param zustelladrFk
     * The zustelladr_fk
     */
    public void setZustelladrFk(Integer zustelladrFk) {
        this.zustelladrFk = zustelladrFk;
    }

    /**
     *
     * @return
     * The ustandortKennz
     */
    public String getUstandortKennz() {
        return ustandortKennz;
    }

    /**
     *
     * @param ustandortKennz
     * The ustandort_kennz
     */
    public void setUstandortKennz(String ustandortKennz) {
        this.ustandortKennz = ustandortKennz;
    }

    /**
     *
     * @return
     * The apartnerAnredeKennz
     */
    public String getApartnerAnredeKennz() {
        return apartnerAnredeKennz;
    }

    /**
     *
     * @param apartnerAnredeKennz
     * The apartner_anrede_kennz
     */
    public void setApartnerAnredeKennz(String apartnerAnredeKennz) {
        this.apartnerAnredeKennz = apartnerAnredeKennz;
    }

    /**
     *
     * @return
     * The apartnerVorname
     */
    public String getApartnerVorname() {
        return apartnerVorname;
    }

    /**
     *
     * @param apartnerVorname
     * The apartner_vorname
     */
    public void setApartnerVorname(String apartnerVorname) {
        this.apartnerVorname = apartnerVorname;
    }

    /**
     *
     * @return
     * The apartnerNachname
     */
    public String getApartnerNachname() {
        return apartnerNachname;
    }

    /**
     *
     * @param apartnerNachname
     * The apartner_nachname
     */
    public void setApartnerNachname(String apartnerNachname) {
        this.apartnerNachname = apartnerNachname;
    }

    /**
     *
     * @return
     * The abholenVon
     */
    public String getAbholenVon() {
        return abholenVon;
    }

    /**
     *
     * @param abholenVon
     * The abholen_von
     */
    public void setAbholenVon(String abholenVon) {
        this.abholenVon = abholenVon;
    }

    /**
     *
     * @return
     * The abholenBis
     */
    public String getAbholenBis() {
        return abholenBis;
    }

    /**
     *
     * @param abholenBis
     * The abholen_bis
     */
    public void setAbholenBis(String abholenBis) {
        this.abholenBis = abholenBis;
    }

    /**
     *
     * @return
     * The zustellenVon
     */
    public String getZustellenVon() {
        return zustellenVon;
    }

    /**
     *
     * @param zustellenVon
     * The zustellen_von
     */
    public void setZustellenVon(String zustellenVon) {
        this.zustellenVon = zustellenVon;
    }

    /**
     *
     * @return
     * The zustellenBis
     */
    public String getZustellenBis() {
        return zustellenBis;
    }

    /**
     *
     * @param zustellenBis
     * The zustellen_bis
     */
    public void setZustellenBis(String zustellenBis) {
        this.zustellenBis = zustellenBis;
    }

    /**
     *
     * @return
     * The zustellenWunsch
     */
    public String getZustellenWunsch() {
        return zustellenWunsch;
    }

    /**
     *
     * @param zustellenWunsch
     * The zustellen_wunsch
     */
    public void setZustellenWunsch(String zustellenWunsch) {
        this.zustellenWunsch = zustellenWunsch;
    }

    /**
     *
     * @return
     * The ibringSpezial
     */
    public String getIbringSpezial() {
        return ibringSpezial;
    }

    /**
     *
     * @param ibringSpezial
     * The ibring_spezial
     */
    public void setIbringSpezial(String ibringSpezial) {
        this.ibringSpezial = ibringSpezial;
    }

    /**
     *
     * @return
     * The auftragsstatus
     */
    public String getAuftragsstatus() {
        return auftragsstatus;
    }

    /**
     *
     * @param auftragsstatus
     * The auftragsstatus
     */
    public void setAuftragsstatus(String auftragsstatus) {
        this.auftragsstatus = auftragsstatus;
    }

    /**
     *
     * @return
     * The orderItems
     */
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    /**
     *
     * @param orderItems
     * The order_items
     */
    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

}